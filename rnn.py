from keras.layers import *
from keras.models import Model

from nn_utils import *


def compile_model(train_conf, window_size):
    units = train_conf["gru_units"]
    conv_size = train_conf["conv_kernel_size"]

    x = input_seq = Input(shape=(window_size,))

    # Encoder
    x = Reshape(target_shape=(window_size, 1))(x)
    x = Conv1D(filters=units, kernel_size=conv_size, padding='same',
               use_bias=False)(x)
    x = CuDNNGRU(units=units, return_sequences=True)(x)
    x = CuDNNGRU(units=units, return_sequences=True)(x)
    bottleneck = CuDNNGRU(units=train_conf["bottleneck_units"])(x)

    # Decoder.
    x = RepeatVector(window_size)(bottleneck)
    x = CuDNNGRU(units=units, return_sequences=True)(x)
    x = CuDNNGRU(units=units, return_sequences=True)(x)
    x = Conv1D(filters=1, kernel_size=conv_size, padding='same',
               use_bias=False)(x)
    n_crop_obs = train_conf["crop_obs"]
    x = Cropping1D(cropping=n_crop_obs)(x)
    predictions = Reshape((window_size - 2 * n_crop_obs,))(x)

    model = Model(inputs=input_seq, outputs=predictions)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]),
                  loss='mean_squared_error')

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    return data_loader.get_signal_from(dataset_desc)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data)
