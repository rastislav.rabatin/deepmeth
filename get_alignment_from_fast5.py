#!/usr/bin/env python3
import h5py
import argparse
import sys

import pandas as pd

from termcolor import colored


parser = argparse.ArgumentParser(description='''
Get alignment of the read with reference sequence from post processed fast5 file with nanoraw.
''')
parser.add_argument('fast5_file', type=str, help='Fast5 read.')
parser.add_argument('alignment_group', type=str, help='''HDF5 group that contains
                    reference alignment. It should contain two datasets -
                    read_alignment and genome_alignment. read_alignment
                    corresponds to the first line in the alignment and
                    genome_alignment corresponds to the second line in the
                    alignemnt of read to the reference sequence.''')
args = parser.parse_args()


LINE_LENGTH = 80
GROUP_SIZE = 10
INDEL_COLOR = 'yellow'
MATCH_COLOR = 'white'
MISMATCH_COLOR = 'red'


def dataset_to_str(dataset):
    return "".join(c.decode("utf-8") for c in dataset)


def get_color(base1, base2):
    if base1 == base2:
        return MATCH_COLOR
    elif base1 != base2 and base1 != '-' and base2 != '-':
        return MISMATCH_COLOR

    return INDEL_COLOR


def color_lines(aln1, aln2):
    """Return tuple of lists. Every list contains colored bases."""
    res = []
    for base1, base2 in zip(aln1, aln2):
        color = get_color(base1, base2)
        res.append((colored(base1, color), colored(base2, color)))

    aln1_colored, aln2_colored = zip(*res)

    return list(aln1_colored), list(aln2_colored)


def split_to_groups(alignment, start_pos):
    """Split alignment line into groups. Returns list of strings.
    Every string is one group.
    """
    return ["".join(alignment[start_pos+group_pos:start_pos+group_pos+GROUP_SIZE])
            for group_pos in range(0, LINE_LENGTH, GROUP_SIZE)]


# Parse Fast5.
with h5py.File(args.fast5_file, 'r') as h5f:
    alignment = h5f[args.alignment_group]
    read_aln = dataset_to_str(alignment["read_alignment"].value)
    ref_aln = dataset_to_str(alignment["genome_alignment"].value)
    read_segs = alignment["read_segments"].value

    clipped_bases_start = alignment.attrs["clipped_bases_start"]
    clipped_bases_end = alignment.attrs["clipped_bases_end"]
    n_deletions = alignment.attrs["num_deletions"]
    n_insertions = alignment.attrs["num_insertions"]
    n_matches = alignment.attrs["num_matches"]
    n_mismatches = alignment.attrs["num_mismatches"]

    ref_start = alignment.attrs["mapped_start"]
    ref_end = alignment.attrs["mapped_end"]

# Compute some statistics about the alignment.
stats_df = pd.DataFrame.from_records([
("Identity", n_matches / len(read_aln) * 100),
("Deletions", n_deletions),
("Insertions", n_insertions),
("Matches", n_matches),
("Mismatches", n_mismatches),
("Totally clipped", clipped_bases_end + clipped_bases_start),
("Cliped from start", clipped_bases_start),
("Cliped from end", clipped_bases_end),
("Reference start", ref_start),
("Reference end", ref_end),
])

print(stats_df.to_string(
    index=False, float_format=lambda x: "{0:g}".format(x)))
print()

print("Lengend")
print("=======")
print("Reference Position")
print("Read")
print("Reference")
print()

print("Group size:", GROUP_SIZE)
print("Line length:", LINE_LENGTH)
print()

# Print the alignment.
for line_start in range(0, len(read_aln), LINE_LENGTH):
    ref_pos = ref_start + line_start
    print(" ".join(["{0:<10d}".format(ref_pos)
        for ref_pos in range(ref_pos, ref_pos+LINE_LENGTH, GROUP_SIZE)]))

    read_aln_colored, ref_aln_colored = color_lines(read_aln, ref_aln)
    line1 = split_to_groups(read_aln_colored, line_start)
    line2 = split_to_groups(ref_aln_colored, line_start)
    print(" ".join(line1))
    print(" ".join(line2))
    print()
