#!/bin/bash

if [ "$#" -ne 4 ]; then
    echo "Usage:"
    echo "./tb_estimate_model.sh [bwa-mem|graphmap|minimap2] \\"
    echo "  fast5_basedir n_jobs model_output_filename"
    exit
fi

aligner=$1
fast5_basedir=$2
n_jobs=$3
output_filename=$4

tombo estimate_kmer_reference \
  --fast5-basedirs $fast5_basedir \
  --tombo-model $output_filename \
  --kmer-specific-sd \
  --upstream-bases 1 \
  --downstream-bases 2 \
  --processes $n_jobs \
  --corrected-group "TomboResquiggle_${aligner}"
