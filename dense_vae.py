from keras.layers import *
from keras.models import Model
from keras.metrics import mse

from nn_utils import *


ACTIVATION = 'tanh'


def encoder(train_conf, window_size):
    enc_input = Input(shape=(window_size,), name='signal_input')

    x = enc_input
    encoder_layers = train_conf["encoder_layer_sizes"]
    for n_units in encoder_layers:
        x = Dense(units=n_units, use_bias=True, activation=ACTIVATION,
                  kernel_initializer="glorot_normal")(x)

    bottleneck = x

    # We don't want to limit the range of mean and log(sigma) so we use
    # linear units. Sigma can actually be only positive but log(sigma) can be
    # positive and also negative.
    z_mean = Dense(units=encoder_layers[-1], use_bias=True, activation='linear',
                   kernel_initializer='glorot_normal')(bottleneck)
    z_log_var = Dense(units=encoder_layers[-1], use_bias=True,
                      activation='linear',
                      kernel_initializer='glorot_normal')(bottleneck)

    return enc_input, z_mean, z_log_var


def build_decoder(train_conf, window_size):
    decoder_layers = train_conf["encoder_layer_sizes"][::-1]
    bottleneck_size = decoder_layers[0]
    decoder_layers = decoder_layers[1:]

    epsilon = Input(shape=(bottleneck_size,))
    z_mean = Input(shape=(bottleneck_size,))
    z_sigma = Input(shape=(bottleneck_size,))

    z = Multiply()([epsilon, z_sigma])
    z = Add()([z, z_mean])

    for n_units in decoder_layers:
        z = Dense(units=n_units, use_bias=True, activation=ACTIVATION,
                  kernel_initializer="glorot_normal")(z)

    n_crop_obs = train_conf["crop_obs"]
    z = Dense(units=window_size - 2 * n_crop_obs, use_bias=True,
              activation="linear", kernel_initializer="glorot_normal")(z)

    return Model(inputs=[epsilon, z_mean, z_sigma], outputs=z)


def sigmoid(x, shift):
    return 1 / (1 + np.exp(-(x - shift)))


class AnnealKLDWeight(Callback):
    def __init__(self, epoch2weight):
        super(AnnealKLDWeight, self).__init__()
        self.logger = logging.getLogger(__name__)
        self.epoch2weight = epoch2weight

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        K.set_value(self.model.kld_weight, self.epoch2weight(epoch))
        self.logger.info("KLD weight set to: %f",
                         K.get_value(self.model.kld_weight))


def kl_loss(z_mean, z_log_var):
    return -0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var),
                        axis=-1)


def vae_loss(z_mean, z_log_var, free_nats, n_lat_groups):
    n_dim = int(z_mean.shape[1])
    group_size = n_dim // n_lat_groups
    assert n_dim % n_lat_groups == 0

    kld_weight = K.variable(0)

    def loss_fn(y_true, y_pred):
        kld = 0
        for i in range(0, n_dim, group_size):
            group_kld = kl_loss(z_mean[:, i:i+group_size],
                                z_log_var[:, i:i+group_size])

            kld += K.maximum(free_nats, group_kld)

        return kld_weight * kld + mse(y_true, y_pred)

    return loss_fn, kld_weight


def compile_model(train_conf, window_size):
    enc_input, z_mean, z_log_var = encoder(train_conf, window_size)
    z_sigma = Lambda(lambda x: K.exp(x / 2), name='exp')(z_log_var)

    x_samples = []
    epsilons = []
    decoder = build_decoder(train_conf, window_size)
    for num in range(train_conf["n_samples"]):
        epsilon = Input(tensor=K.random_normal(shape=K.shape(z_mean)),
                        name="epsilon{:03}".format(num))
        x_samples.append(decoder([epsilon, z_mean, z_sigma]))
        epsilons.append(epsilon)

    if train_conf["n_samples"] > 1:
        x_mean_est = Average()(x_samples)
    else:
        x_mean_est = x_samples[0]

    loss, kld_weight = vae_loss(z_mean, z_log_var,
                                train_conf.get("free_nats", 0.0),
                                train_conf.get("n_lat_groups", 1))
    model = Model(inputs=[enc_input] + epsilons, outputs=x_mean_est)
    model.kld_weight = kld_weight
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]), loss=loss,
                  metrics=[mse])

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    return data_loader.get_signal_from(dataset_desc)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    callbacks = []
    if train_conf.get("use_annealing", False):
        schedule = train_conf.get("schedule")
        schedule_fn = globals()[schedule["name"]]
        schedule_fn = partial(schedule_fn, **schedule["args"])
        callbacks.append(AnnealKLDWeight(schedule_fn))

    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data, callbacks)
