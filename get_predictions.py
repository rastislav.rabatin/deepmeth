#!/usr/bin/env python3
import sys
import argparse

from keras.models import Model

from nn_utils import *
from data_loader import get_all_dataset_names


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Load particular checkpoint and get predictions of the model. Save 
        predictions to predictions to model_dir/predictions/epoch_XXXXX/*  
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('--epoch', type=int,
                        help='Checkpoint epoch number starting from one. '
                             'If no epoch is specified then use the last '
                             'epoch. (1-based index)')
    parser.add_argument('--min_val_loss', action='store_true',
                        dest='min_val_loss', default=False,
                        help='Choose checkpoint with minimum validation error.')
    parser.add_argument('--no_bottleneck', dest='bottleneck',
                        action='store_false', default=True,
                        help='Do not store bottleneck features in '
                             'output_dir/bottleneck/epoch_XXXXX/*')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    return parser.parse_args()


def get_mse_for(dataset_desc):
    X, y_true = get_data_for_prediction(dataset_desc, data_loader,
                                        train_conf, model_dir)
    output = model.predict(X, verbose=True,
                           batch_size=train_conf["batch_size"])

    path_suffix = get_dataset_path_suffix(dataset_desc)
    if args.bottleneck:
        y_pred = output[0]
        path = bottleneck_dir + path_suffix
        os.makedirs(os.path.dirname(path), exist_ok=True)
        np.savez_compressed(path, output[1])
        logging.info("Saved bottleneck features to %s.npz", path)
    else:
        y_pred = output

    y_pred = np.reshape(y_pred, y_pred.shape[:2])
    y_true = np.reshape(y_true, y_true.shape[:2])
    path = predictions_dir + path_suffix
    os.makedirs(os.path.dirname(path), exist_ok=True)
    np.savez_compressed(path, y_pred)
    logging.info("Saved predictions to %s.npz", path)

    return ((y_true - y_pred)**2).mean(axis=1).mean(axis=0)


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)

    apply_tf_settings(train_conf["intra_op_threads"],
                      train_conf["inter_op_threads"])

    model_module = __import__(train_conf["model_module"])
    get_data_for_prediction = getattr(model_module, 'prepare_data')

    # Load model.
    model_dir = args.model_dir
    model_name = model_dir.split('/')[-1]
    if args.min_val_loss:
        val_loss, args.epoch = get_epoch_with_min_val_loss(model_dir)
        logging.info("Model with min val loss is from epoch %d.", args.epoch)

    if args.epoch is None:
        # This is because of backward compatibility. It can load also only
        # weights files without optimizer state.
        model, args.epoch = load_last_model(model_dir,
                                            train_conf["learning_rate"])
    else:
        model = load_model_from(model_dir, args.epoch,
                                train_conf["learning_rate"])
    logging.info("Loaded model from epoch %d.", args.epoch)

    # Create predictions directory structure.
    epoch_str = epoch_num_to_str(args.epoch)
    predictions_dir = "{}/predictions/{}/{}".format(split_conf["output_dir"],
                                                    model_name, epoch_str)

    input_tensors = model.inputs
    output_tensors = model.outputs

    bottleneck_dir = "{}/bottleneck/{}/{}".format(split_conf["output_dir"],
                                                  model_name, epoch_str)
    # Find bottleneck layer.
    if args.bottleneck:
        bottleneck_idx = np.argmin([
            np.prod([s for s in layer.output_shape if s is not None])
            for layer in model.layers
            if not layer.get_config()["name"].startswith("input")
        ])
        bottleneck_layer = model.layers[bottleneck_idx]
        # We want to get autoencoder output and bottleneck layers output in
        # one prediction.
        output_tensors.append(bottleneck_layer.output)

        logging.info("Bottleneck layer name: %s; index: %d; output_shape: %s",
                     bottleneck_layer.name, bottleneck_idx,
                     bottleneck_layer.output_shape)

    datasets_mse = []
    model = Model(inputs=input_tensors, outputs=output_tensors)

    # Reshape input only in case when model input is 3D tensor.
    # This is because of compatibility reasons with old networks.
    data_loader = DataLoader(
        split_conf, train_conf["crop_obs"],
        reshape_for_keras=len(model.inputs[0].shape) == 3)

    datasets = get_all_dataset_names(split_conf)
    for dataset_name, dataset_type, meth_id in datasets:
        dataset_desc = DatasetDesc(dataset_name, dataset_type, meth_id)
        datasets_mse.append((dataset_name, get_mse_for(dataset_desc)))

    pd.DataFrame\
      .from_records(sorted(datasets_mse), columns=["dataset", "MSE"])\
      .to_csv(predictions_dir + "/summary_errors.csv", index=False)
