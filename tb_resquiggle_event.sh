#!/bin/bash

if [ "$#" -ne 5 ]; then
    echo "Usage:"
    echo "./tb_resquiggle.sh [bwa-mem|graphmap|minimap2] \\"
    echo "  failed_reads_filename n_jobs fast5_basedir ref_fasta"
    exit
fi

aligner=$1
failed_reads_filename=$2
n_jobs=$3
fast5_basedir=$4
ref_fasta=$5

tombo event_resquiggle \
    --${aligner}-executable $aligner \
    --failed-reads-filename $failed_reads_filename \
    --include-event-stdev \
    --processes $n_jobs \
    --corrected-group "TomboEventResquiggle_${aligner}"\
    --overwrite \
    --normalization-type median \
    $fast5_basedir $ref_fasta
