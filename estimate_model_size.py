#!/usr/bin/env python3
import argparse
import numpy as np

from nn_utils import *
from keras import backend as K


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Estimate RAM consumption that the keras model needs.
        Based on https://stackoverflow.com/questions/43137288/how-to-determine-needed-memory-of-keras-model
    ''')
    parser.add_argument('--split_conf', type=str, default="split_conf.yaml",
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str, default="train_conf.yaml",
                        help='Yaml config file with training configuration.')
    parser.add_argument('--batch_size', type=int,
                        help='Override batch_size from config with this value.')
    return parser.parse_args()


def get_model_memory_usage(batch_size, model):
    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p)
                              for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p)
                                  for p in set(model.non_trainable_weights)])

    total_memory = 4.0*batch_size*(shapes_mem_count + trainable_count +
                                   non_trainable_count)
    gbytes = np.round(total_memory / (1024.0 ** 3), 3)
    return gbytes


if __name__ == "__main__":
    args = parse_args()
    split_conf, train_conf = get_configs(args.split_conf, args.train_conf)
    model_module = __import__(train_conf["model_module"])
    compile_model = getattr(model_module, 'compile_model')
    model = compile_model(train_conf, split_conf["window_size"])

    model.summary()

    batch_size = train_conf["batch_size"]
    if hasattr(args, "batch_size"):
        batch_size = args.batch_size
    memory = get_model_memory_usage(batch_size, model)
    print("This model needs:", memory, "GB")
