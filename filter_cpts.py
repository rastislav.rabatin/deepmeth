#!/usr/bin/env python3
import sys
import argparse

from data_loader import *


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Filter out checkpoints which do not improve validation error.
        Keep last ten checkpoints.
    ''')
    parser.add_argument('directory', type=str,
                        help='Path to directory with models or directory with '
                             'a particular model.')
    parser.add_argument('--dry_run', dest='dry_run',
                        action='store_true', default=False,
                        help='Do not remove the files. Just produce the list '
                             'of files which should be deleted.')
    parser.add_argument('--keep_last_n', type=int, default=10,
                        help='Keep last n epochs.')
    return parser.parse_args()


def process(model_dir):
    epochs_file = model_dir + "/epochs.csv"
    if not os.path.isfile(epochs_file):
        logging.info("%s: does not contain epochs.csv", model_dir)
        return None

    cpt_dir = "{}/checkpoints".format(model_dir)
    epoch_to_path = {}
    for file in os.listdir(cpt_dir):
        if not file.endswith(".hdf5"):
            continue

        path = "{}/{}".format(cpt_dir, file)
        epoch_num = get_epoch_num_from(file)

        # Epochs in checkpoint file names are 1-based indexed and epochs in
        # epoch df are 0-based indexed.
        epoch_to_path[epoch_num - 1] = path

    try:
        df = pd.read_csv(epochs_file)
    except Exception as e:
        logging.warning("%s: %s", epochs_file, e)
        return None

    epoch_to_val_loss = {}
    for _, row in df.iterrows():
        epoch = row["epoch"]
        # This is probably because the training was killed during writing to
        # the file.
        if np.isnan(epoch):
            continue

        epoch_to_val_loss[int(epoch)] = row["val_loss"]

    keep_files = []
    delete_files = []
    min_val_loss = 1000
    epoch_min_val_loss = -1
    n_epochs = len(epoch_to_path)
    for epoch, path in epoch_to_path.items():
        # Keep the last N epochs even if they are not in epochs.csv.
        # The training might be in progress in this case.
        if epoch >= n_epochs - args.keep_last_n:
            keep_files.append(path)
            if epoch_to_val_loss.get(epoch, 1000) < min_val_loss:
                min_val_loss = epoch_to_val_loss[epoch]
                epoch_min_val_loss = epoch

            continue

        # This might mean that the training crashed during the checkpointing.
        if epoch not in epoch_to_val_loss:
            delete_files.append(path)
            continue

        # Keep epoch if the val_loss improved.
        val_loss = epoch_to_val_loss[epoch]
        if val_loss < min_val_loss:
            keep_files.append(path)
            min_val_loss = val_loss
            epoch_min_val_loss = epoch
        else:
            delete_files.append(path)

    if epoch_min_val_loss != -1:
        model_best_epoch.append((model_dir, epoch_min_val_loss+1,
                                 min_val_loss, len(delete_files),
                                 len(keep_files)))
    else:
        logging.info("No epochs for %s", model_dir)

    return keep_files, delete_files


def total_size(files):
    return sum(os.path.getsize(f) for f in files)


def save_path_list(filename, paths):
    with open(filename, "w+") as f:
        f.write("\n".join(paths))


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    delete_files = []
    keep_files = []
    model_best_epoch = []

    abs_path = os.path.abspath(args.directory)

    if "checkpoints" in os.listdir(abs_path):
        res = process(abs_path)
        if res is not None:
            keep_files += res[0]
            delete_files += res[1]
    else:
        for dir in os.listdir(abs_path):
            path = abs_path + "/" + dir
            if not os.path.isdir(path) or "checkpoints" not in os.listdir(path):
                continue

            res = process(path)
            if res is not None:
                keep_files += res[0]
                delete_files += res[1]

    delete_size = total_size(delete_files)//10**6
    keep_size = total_size(keep_files)//10**6
    print("Total size to delete: {:,} MB".format(delete_size))
    print("Total size to keep: {:,} MB".format(keep_size))

    save_path_list("delete_cpts.txt", delete_files)
    save_path_list("keep_cpts.txt", keep_files)

    COLS = ["model_dir", "Min val loss epoch (1-based index)", "val_loss",
            "cpts_del", "cpts_keep"]
    pd.DataFrame.from_records(model_best_epoch, columns=COLS) \
                .to_csv("model_to_best_epoch.csv", index=False)

    if not args.dry_run:
        for path in delete_files:
            os.remove(path)
