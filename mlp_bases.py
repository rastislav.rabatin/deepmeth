""" Autoencoder with bases added into bottleneck and Dense layers. """

from keras.layers import *
from keras.models import Model

from nn_utils import *

ACTIVATION = 'tanh'


def compile_model(train_conf, window_size):
    add_bases = train_conf.get("add_bases", True)

    sig = Input(shape=(window_size,))
    inputs = [sig]

    bases = None
    if add_bases:
        kmer_size = 2 * train_conf["n_bases_around"] + 1
        bases = Input(shape=(kmer_size * ALPHABET_SIZE,))
        inputs.append(bases)

    x = sig
    encoder_layers = train_conf["encoder_layer_sizes"]
    for n_units in encoder_layers:
        x = Dense(units=n_units, use_bias=True, activation=ACTIVATION,
                  kernel_initializer="glorot_normal")(x)

    # Bottleneck
    if add_bases:
        x = Concatenate()([x, bases])

    # Decoder has the same layers in the reversed order.
    # Skip the bottleneck layer.
    for n_units in encoder_layers[-2::-1]:
        x = Dense(units=n_units, use_bias=True, activation=ACTIVATION,
                  kernel_initializer="glorot_normal")(x)

    n_crop_obs = train_conf["crop_obs"]
    predictions = Dense(units=window_size - 2 * n_crop_obs, use_bias=True,
                        activation="linear",
                        kernel_initializer="glorot_normal")(x)

    model = Model(inputs=inputs, outputs=predictions)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]),
                  loss='mean_squared_error')

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    if train_conf.get("add_bases", True):
        return prepare_signal_with_bases(dataset_desc, data_loader, train_conf,
                                         model_dir)
    else:
        return data_loader.get_signal_from(dataset_desc)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data)
