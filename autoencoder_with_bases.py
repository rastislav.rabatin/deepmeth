from keras.layers import *
from keras.models import Model

from nn_utils import *

KERNEL_SIZE = 19
ACTIVATION = 'elu'


def apply_conv_module(x, n_filters, strides):
    x = Conv1D(filters=n_filters, kernel_size=KERNEL_SIZE, strides=strides,
               padding='same', kernel_initializer='glorot_normal',
               use_bias=False)(x)
    x = BatchNormalization()(x)
    return Activation(ACTIVATION)(x)


def compile_model(train_conf, window_size):
    sig = Input(shape=(window_size,))
    kmer_size = 2 * train_conf["n_bases_around"] + 1
    bases = Input(shape=(kmer_size * ALPHABET_SIZE,))

    x = Concatenate()([sig, bases])
    x = Dense(window_size, activation=ACTIVATION)(x)

    # We need to reshape the output because of the Conv1D.
    x = Reshape(target_shape=(window_size, 1))(x)

    # Encoder convolutional modules.
    n_conv_modules = train_conf.get("n_conv_modules", 4)
    for _ in range(n_conv_modules):
        x = apply_conv_module(x, train_conf["filters"], 2)

    x = Flatten()(x)
    # Bottleneck layer.
    bottleneck_size = window_size // 2 ** n_conv_modules
    x = Dense(units=bottleneck_size, use_bias=True,
              activation=ACTIVATION, kernel_initializer='glorot_normal')(x)

    # Decoder
    if train_conf.get("add_bases_to_neck", True):
        x = Concatenate()([x, bases])
    x = Dense(units=bottleneck_size * train_conf["filters"], use_bias=True,
              activation=ACTIVATION, kernel_initializer='glorot_normal')(x)
    x = Reshape(target_shape=(bottleneck_size, train_conf["filters"]))(x)

    # Deconvolutional modules.
    for _ in range(n_conv_modules):
        x = apply_conv_module(x, train_conf["filters"], 1)
        x = UpSampling1D(size=2)(x)

    # Last convolutional layer with only one filter. The shape of x should be
    # (window_size, train_conf["filters"]).
    x = Conv1D(filters=1, kernel_size=KERNEL_SIZE, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = Activation('linear', name='linear')(x)

    n_crop_obs = train_conf["crop_obs"]
    x = Cropping1D(cropping=n_crop_obs)(x)
    predictions = Reshape((window_size - 2 * n_crop_obs,))(x)

    model = Model(inputs=[sig, bases], outputs=predictions)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]),
                  loss='mean_squared_error')

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    return prepare_signal_with_bases(dataset_desc, data_loader, train_conf,
                                     model_dir)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data)
