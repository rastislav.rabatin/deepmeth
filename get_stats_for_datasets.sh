#!/bin/bash

# Extract statistics for every data set using poretools stats.

DATA_DIR='/extdata/nanoraw-data'

cat $DATA_DIR/fast5_dirs.list | \
parallel --eta "poretools stats --full-tsv $DATA_DIR/{}/ > $DATA_DIR/{}.full_stats"
