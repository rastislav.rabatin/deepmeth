#!/bin/bash

run_scappie() {
    DATA_DIR='/extdata/nanoraw-data'
    fast5_file="$DATA_DIR/$1"
    scrappie_raw_fasta_dir="$DATA_DIR/$(dirname $1)_scrappie_raw_fasta_dir"
    scrappie_events_fasta_dir="$DATA_DIR/$(dirname $1)_scrappie_events_fasta_dir"
    mkdir -p $scrappie_raw_fasta_dir
    mkdir -p $scrappie_events_fasta_dir
    filename=$(basename $1 .fast5)
    scrappie raw $fast5_file > $scrappie_raw_fasta_dir/$filename.fasta
    scrappie events $fast5_file > $scrappie_events_fasta_dir/$filename.fasta
}

export -f run_scappie

cat $1 | parallel --eta run_scappie
