#!/usr/bin/env python3
import h5py
import argparse
import logging
import sys
import os


parser = argparse.ArgumentParser(
    description='Rename fast5 files to <uuid>.fast5.')
parser.add_argument('fast5_files_list', type=str,
                    help='File with list of fast5 files')
parser.add_argument('--base_dir', type=str, default=None,
    help='Base directory to append to every path in the list of files.')
args = parser.parse_args()


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


def rename(path):
    UUID_PATH = '/Analyses/Basecall_1D_000/Configuration/general'

    h5f = None
    uuid = None
    try:
        h5f = h5py.File(path, 'r')
        uuid = h5f[UUID_PATH].attrs['uuid'].decode("utf-8")
    except Exception as e:
        logging.warn("Failed to parse: %s; Exception: %s", path, e)
    finally:
        if h5f is not None: 
            h5f.close()

    if uuid is not None:
        new_name = "/".join(path.split("/")[:-1]) + "/" + uuid + ".fast5"
        os.rename(path, new_name)
        logging.info("Successfully renamed %s to %s", path, new_name)


base_dir = ""
if args.base_dir is not None:
    base_dir = args.base_dir + "/"
files = [base_dir + line.strip()
         for line in open(args.fast5_files_list, 'r')
         if line.strip().endswith(".fast5")]

for f in files:
    rename(f)
