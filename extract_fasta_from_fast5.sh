#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Extract forward strand from every fast5 file to fasta."
    echo "Usage:"
    echo "./get_fasta.sh [number of jobs] [file with list of fast5 files]"
    exit
fi

extract_fasta() {
    fast5_file=$1
    fasta_file=$(echo $fast5_file | sed 's/fast5/fasta/g')
    fasta_dir=$(dirname $fasta_file)
    read_id=$(basename $fasta_file .fasta)
    mkdir -p $fasta_dir
    poretools fasta --type=fwd $fast5_file | \
    sed 's|>.*$|>'$read_id'|g' > $fasta_file
}

export -f extract_fasta

cat $2 | parallel -j $1 --eta extract_fasta
