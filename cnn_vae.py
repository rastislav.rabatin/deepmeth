from keras.layers import *
from keras.models import Model
from keras.metrics import mse

from nn_utils import *


def apply_conv_module(x, n_filters):
    x = Conv1D(filters=n_filters, kernel_size=8, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = BatchNormalization()(x)
    return Activation('elu')(x)


def encoder(train_conf, window_size):
    enc_input = Input(shape=(window_size,), name='signal_input')
    x = Reshape(target_shape=(window_size, 1))(enc_input)

    # Encoder convolutional modules.
    n_conv_modules = train_conf.get("n_conv_modules", 4)
    for _ in range(n_conv_modules):
        x = apply_conv_module(x, train_conf["filters"])
        x = AveragePooling1D(pool_size=2, strides=2, padding='same')(x)

    x = Flatten()(x)
    # Bottleneck layer.
    bottleneck_size = window_size // 2 ** n_conv_modules
    bottleneck = Dense(units=bottleneck_size, use_bias=True, activation='elu',
                       kernel_initializer='glorot_normal')(x)

    # We don't want to limit the range of mean and log(sigma) so we use
    # linear units. Sigma can actually be only positive but log(sigma) can be
    # positive and also negative.
    z_mean = Dense(units=bottleneck_size, use_bias=True, activation='linear',
                   kernel_initializer='glorot_normal')(bottleneck)
    z_log_var = Dense(units=bottleneck_size, use_bias=True, activation='linear',
                      kernel_initializer='glorot_normal')(bottleneck)

    return enc_input, z_mean, z_log_var


def decoder(train_conf, window_size, z):
    n_conv_modules = train_conf.get("n_conv_modules", 4)
    bottleneck_size = window_size // 2 ** n_conv_modules

    x = Dense(units=bottleneck_size * train_conf["filters"], use_bias=True,
              activation='elu', kernel_initializer='glorot_normal')(z)
    x = Reshape(target_shape=(bottleneck_size, train_conf["filters"]))(x)

    # Deconvolutional modules.
    for _ in range(n_conv_modules):
        x = apply_conv_module(x, train_conf["filters"])
        x = UpSampling1D(size=2)(x)

    x = Conv1D(filters=1, kernel_size=8, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = Reshape((window_size,))(x)

    n_crop_obs = train_conf["crop_obs"]
    output_size = window_size - 2 * n_crop_obs

    x_mean = Dense(units=output_size, use_bias=True, activation='linear',
                   kernel_initializer='glorot_normal')(x)

    return x_mean


def sample_from(z_mean, z_log_var):
    epsilon = Input(tensor=K.random_normal(shape=K.shape(z_mean)),
                    name='epsilon')
    z_sigma = Lambda(lambda x: K.exp(x / 2), name='exp')(z_log_var)
    z_epsilon = Multiply()([z_sigma, epsilon])
    return epsilon, Add()([z_mean, z_epsilon])


def kl_loss(z_mean, z_log_var):
    return -0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var),
                        axis=-1)


def vae_loss(z_mean, z_log_var):
    def loss_fn(y_true, y_pred):
        return kl_loss(z_mean, z_log_var) + mse(y_true, y_pred)

    return loss_fn


def compile_model(train_conf, window_size):
    enc_input, z_mean, z_log_var = encoder(train_conf, window_size)
    epsilon, z = sample_from(z_mean, z_log_var)
    x_mean = decoder(train_conf, window_size, z)

    loss = vae_loss(z_mean, z_log_var)
    model = Model(inputs=[enc_input, epsilon], outputs=x_mean)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]), loss=loss,
                  metrics=[mse])

    return model


class AnnealKLDWeight(Callback):
    def __init__(self):
        super(AnnealKLDWeight, self).__init__()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        K.set_value(self.model.kld_weight,
                    min(K.get_value(self.model.kld_weight)*1.1, 1))


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    return data_loader.get_signal_from(dataset_desc)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data,
                                  additional_callbacks=[AnnealKLDWeight()])
