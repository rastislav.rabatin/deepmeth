#!/usr/bin/env python3
import sys
import logging
import argparse
import numpy as np
import pandas as pd

from tqdm import tqdm
from collections import defaultdict
from data_loader import ReadLoader, DatasetDesc


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Estimate k-mer model for the signal mean levels.
    ''')
    parser.add_argument('split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('model_name', type=str,
                        help='Model name. The models is going to be stored '
                             'in model_name.csv.')
    parser.add_argument('--left_bases', type=int, default=2,
                        help='Number of bases to the left of the middle base.')
    parser.add_argument('--right_bases', type=int, default=3,
                        help='Number of bases to the right of the middle base.')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(stream=sys.stdout, level=logging.WARNING)

    left_bases = args.left_bases
    right_bases = args.right_bases

    read_loader = ReadLoader(args.split_conf)

    # Train on PCR samples.
    kmer_to_obs = defaultdict(list)
    dataset_desc = DatasetDesc("train", "train", None)
    n_reads = read_loader.num_reads(dataset_desc)
    print("The training set has {} reads.".format(n_reads))

    for bases_dict, seg_starts, signal in \
            tqdm(read_loader.iterate_reads(dataset_desc), total=n_reads):
        assert seg_starts[0] == 0

        base_seq = bases_dict["base_seq"]
        for mid_pos in range(left_bases, len(seg_starts) - right_bases):
            start = seg_starts[mid_pos]
            end = seg_starts[mid_pos + 1]
            mean_level = signal[start:end].mean()
            kmer = base_seq[mid_pos - left_bases:mid_pos + right_bases + 1]
            kmer_to_obs[kmer].append(mean_level)

    model = []
    n_kmers = len(kmer_to_obs)
    print("Number of k-mers with at least one occurrence:", n_kmers)
    for kmer, obs in tqdm(kmer_to_obs.items(), total=n_kmers):
        assert len(kmer) == left_bases + right_bases + 1

        model.append([kmer, np.mean(obs, dtype=np.float64),
                      np.std(obs, dtype=np.float64), len(obs)])

    df = pd.DataFrame.from_records(model, columns=["kmer", "mean", "std",
                                                   "n_samples"])
    df.to_csv(args.model_name + ".csv", index=False)
