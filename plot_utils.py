import os


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from data_loader import METH_PATTERNS

from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.metrics import precision_recall_curve, average_precision_score


METH_TO_COLOR = {
    "meth01": "blue",
    "meth02": "orange",
    "meth04": "green",
    "meth08": "red",
    "meth09": "magenta",
    "meth10": "brown",
    "meth11": "cyan"
}


def pyplot_configs():
    params = {
        'axes.labelsize': 12,  # fontsize for x and y labels (was 10)
        'axes.titlesize': 12,
        'font.size': 12,  # was 10
        'legend.fontsize': 9,  # was 10
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'font.family': 'serif',
        'figure.titlesize': 20,
        'savefig.dpi': 300,
        'grid.linestyle': '--',
        'legend.frameon': True,
    }

    plt.style.use('seaborn-white')

    plt.rcParams.update(params)


def get_title(dataset_name):
    meth_df = pd.read_csv(os.environ["SCRIPTS"] + "/methylases.csv")
    title = dataset_name
    if (meth_df["Identifier"] == dataset_name).any():
        row = meth_df[meth_df["Identifier"] == dataset_name]
        title = "{} ({}) {} {}".format(
            row["Identifier"].iloc[0], row["Library"].iloc[0],
            row["Recognition site"].iloc[0], row["Methylase class"].iloc[0])

    return title


def get_pretty_meth_label(col_name):
    if not col_name.startswith("meth"):
        return col_name

    meth_df = pd.read_csv(os.environ["SCRIPTS"] + "/methylases.csv")
    meth_group = col_name.replace("_loss", "")
    meth_row = meth_df[meth_df["Identifier"] == meth_group]

    return "{} {} - {}".format(meth_row["Recognition site"].values[0],
                               meth_row["Methylase class"].values[0],
                               meth_group.replace("meth", "m"))


def plot_one_roc(meth_id, df, show_num_samples, invert_auc):
    scores = df.score.values
    y_true = df.is_methylated

    scores[np.isposinf(scores)] = np.finfo(np.float32).max

    if np.all(y_true) or np.all(~y_true):
        return None

    auc = roc_auc_score(y_true, scores)

    # Flip label if AUC < 0.5. Do this only when we use mean reconstruction
    # error. It can be bigger or higher than normal samples. For log
    # likelihood we do not want to invert it.
    if invert_auc and auc < 0.5:
        auc = 1 - auc
        y_true = ~y_true

    fpr, tpr, _ = roc_curve(y_true, scores)

    n_positive = np.sum(y_true)
    label = "{0} (AUC = {1:.2f}".format(get_pretty_meth_label(meth_id), auc)
    if show_num_samples:
        label += "; pos = {}; neg = {})".format(n_positive,
                                                len(y_true) - n_positive)
    else:
        label += ")"

    plt.plot(fpr, tpr, label=label, color=METH_TO_COLOR[meth_id])

    return (meth_id, METH_PATTERNS[meth_id], auc, n_positive,
            len(y_true) - n_positive, len(y_true))


def plot_roc(df, show_num_samples=False, invert_auc=False):
    plt.grid(True)

    COLS = ["meth_id", "pattern", "roc_auc", "pos_samples", "neg_samples",
            "n_samples"]

    rows = []
    for meth_id, group in df.groupby("meth_id"):
        row = plot_one_roc(meth_id, group, show_num_samples, invert_auc)
        if row is None:
            continue

        assert len(row) == len(COLS)
        rows.append(row)

    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])

    plt.xticks(np.linspace(0, 1, 11))
    plt.yticks(np.linspace(0, 1, 11))

    minor_locator = AutoMinorLocator(2)
    plt.axes().xaxis.set_minor_locator(minor_locator)
    plt.axes().yaxis.set_minor_locator(minor_locator)
    plt.grid(which='minor')

    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC')
    plt.legend(loc="lower right")

    return pd.DataFrame.from_records(rows, columns=COLS)


def plot_one_pr_curve(meth_id, df, show_num_samples):
    scores = df.score.values
    y_true = df.is_methylated

    scores[np.isposinf(scores)] = np.finfo(np.float32).max

    if np.all(y_true) or np.all(~y_true):
        return None

    pr_auc = average_precision_score(y_true, scores)
    precision, recall, _ = precision_recall_curve(y_true, scores)

    n_positive = np.sum(y_true)
    label = "{0} (PR-AUC = {1:.2f}".format(get_pretty_meth_label(meth_id),
                                           pr_auc)
    if show_num_samples:
        label += "; pos = {}; neg = {})".format(n_positive,
                                                len(y_true) - n_positive)
    else:
        label += ")"

    plt.plot(recall, precision, label=label, color=METH_TO_COLOR[meth_id])

    return (meth_id, METH_PATTERNS[meth_id], pr_auc, n_positive,
            len(y_true) - n_positive, len(y_true))


def plot_precision_recall(df, show_num_samples=False):
    plt.grid(True)

    COLS = ["meth_id", "pattern", "pr_auc", "pos_samples", "neg_samples",
            "n_samples"]

    rows = []
    for meth_id, group in df.groupby("meth_id"):
        row = plot_one_pr_curve(meth_id, group, show_num_samples)
        if row is None:
            continue

        assert len(row) == len(COLS)
        rows.append(row)

        pos_ratio = row[3] / (row[3] + row[4])
        plt.plot([0, 1], [pos_ratio, pos_ratio], color=METH_TO_COLOR[meth_id],
                 linestyle='--')

    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])

    plt.xticks(np.linspace(0, 1, 11))
    plt.yticks(np.linspace(0, 1, 11))

    minor_locator = AutoMinorLocator(2)
    plt.axes().xaxis.set_minor_locator(minor_locator)
    plt.axes().yaxis.set_minor_locator(minor_locator)
    plt.grid(which='minor')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Precision-recall')
    plt.legend(loc="upper left")

    return pd.DataFrame.from_records(rows, columns=COLS)


def plot_score_vs_coverage(site_auc_df, score_col_name):
    plt.grid(True)

    max_cov = site_auc_df.coverage.max()
    ticks = list(range(1, max_cov + 1))
    for meth_id, group in site_auc_df.groupby("meth_id"):
        scores = group.sort_values("coverage")[score_col_name].values
        plt.plot(ticks[:len(scores)], scores, color=METH_TO_COLOR[meth_id],
                 label=get_pretty_meth_label(meth_id))

    plt.xticks(ticks)
    plt.yticks(np.linspace(0, 1, 11))

    minor_locator = AutoMinorLocator(2)
    plt.axes().yaxis.set_minor_locator(minor_locator)
    plt.grid(which='minor')

    plt.ylim((0, 1))
    plt.xlabel("Coverage")
    plt.legend(loc="lower left")
