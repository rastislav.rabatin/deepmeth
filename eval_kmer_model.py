#!/usr/bin/env python3
import sys
import argparse
from shutil import copyfile

from scores_utils import *
from utils import strand_to_full_str

from tqdm import tqdm
from multiprocessing import Pool
from scipy.stats import norm


def parse_args():
    parser = argparse.ArgumentParser(
        description='''
            Detect methylated windows using kmer model.
        ''',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('kmer_model', type=str,
                        help='CSV file with k-mer model.')
    parser.add_argument('split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--strands', nargs="+", default=["+", "-", "both"],
                        help='Strands for which to evaluate the models.')
    parser.add_argument('--left_bases', type=int, default=2,
                        help='Number of bases to the left of the middle base.')
    parser.add_argument('--right_bases', type=int, default=3,
                        help='Number of bases to the right of the middle base.')
    parser.add_argument('--first_center', type=int, default=3,
                        help='The first base to the left that is going to be '
                             'used as a center of k-mer.')
    parser.add_argument('--last_center', type=int, default=2,
                        help='The last base to the right that is going'
                             ' to be used as a center o k-mer.')
    return parser.parse_args()


def get_data_for(dataset_desc):
    X, _ = data_loader.get_signal_from(dataset_desc)
    bases_df = data_loader.get_bases_from(dataset_desc)
    seg_starts = data_loader.get_seg_starts_from(dataset_desc)

    skip_sample = filter_windows(seg_starts, X.shape[1])
    n_samples = len(skip_sample)
    n_remove = np.sum(skip_sample)
    logging.warning("Skipping %d (%f %% -- %d) samples for %s", n_remove,
                    (n_remove / n_samples) * 100, n_samples, dataset_desc)

    X = X[~skip_sample]
    bases_df = bases_df.iloc[~skip_sample, :]
    seg_starts = seg_starts[~skip_sample]

    tb_stats_csv_path = split_conf["tombo_stats_csv"]
    bases_df = add_pattern_ref_pos(bases_df, dataset_desc.meth_id,
                                   tb_stats_csv_path)

    return X, bases_df, seg_starts


def get_scores(meth_id, signal, base_seqs, seg_starts):
    motif = METH_PATTERNS[meth_id]
    motif_pos = METHYLATED_POSITIONS[meth_id]

    left_bases = args.left_bases
    right_bases = args.right_bases
    kmer_size = left_bases + right_bases + 1

    res = np.zeros((signal.shape[0],))
    skip_sample = [False for _ in range(signal.shape[0])]
    for pos, (sig, seq, starts) in enumerate(zip(signal, base_seqs,
                                                 seg_starts)):
        meth_pos = seq.find(motif) + motif_pos

        score = 0
        for center_pos in range(meth_pos - args.first_center,
                                meth_pos + args.last_center + 1):
            start = starts[center_pos]
            end = starts[center_pos + 1]
            mean_lvl = sig[start:end].mean()
            cur_kmer = seq[center_pos - left_bases:center_pos + right_bases + 1]

            # Skip samples for which the sliding k-mer window goes out of the
            # input window.
            if len(cur_kmer) != kmer_size:
                skip_sample[pos] = True
                break

            score += norm.logpdf(mean_lvl, *kmer_model[cur_kmer])

        res[pos] = score

    return res, np.array(skip_sample)


def scores_for_strand(meth_id, sig, bases_df, seg_starts):
    is_meth = bases_df.is_methylated.values
    scores, skip_sample = get_scores(meth_id, sig, bases_df.base_seq,
                                     seg_starts)
    sizes = [len(is_meth) - np.sum(is_meth), np.sum(is_meth)]
    df = pd.DataFrame.from_dict({
        'score': -scores,
        'pattern_ref_pos': bases_df.pattern_ref_pos,
        'dataset_name': np.repeat([meth_id + "_pcr", meth_id], sizes),
        'is_methylated': is_meth,
    })

    df["meth_id"] = meth_id

    df = df.iloc[~skip_sample, :]

    n_samples = len(skip_sample)
    n_skip = np.sum(skip_sample)
    logging.warning("Skipped %d (%f %% -- %d) samples for %s.", n_skip,
                    (n_skip / n_samples) * 100, n_samples, meth_id)

    return df


def select_data_for(strand, sig, bases_df, seg_starts):
    mask = bases_df.strand.values == strand

    segs = [sample for sample, use in zip(seg_starts, mask) if use]
    return sig[mask, :], bases_df.iloc[mask, :], segs


def compute_scores_for(meth_id):
    pcr_data = get_data_for(DatasetDesc("dev", "dev", meth_id))
    meth_data = get_data_for(DatasetDesc(meth_id, "dev", meth_id))

    pcr_data[1]["is_methylated"] = False
    meth_data[1]["is_methylated"] = True

    sig = np.concatenate((pcr_data[0], meth_data[0]))
    bases_df = concatenate_dfs([pcr_data[1], meth_data[1]])
    seg_starts = np.concatenate((pcr_data[2], meth_data[2]))

    res = {}
    for strand in ["+", "-"]:
        if strand not in args.strands:
            continue

        strand_data = select_data_for(strand, sig, bases_df, seg_starts)
        res[strand] = scores_for_strand(meth_id, *strand_data)

    if "both" in args.strands:
        res["both"] = scores_for_strand(meth_id, *strand_data)

    return res


if __name__ == "__main__":
    args = parse_args()
    pyplot_configs()
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    left_bases = args.left_bases
    right_bases = args.right_bases
    kmer_size = left_bases + right_bases + 1
    model_name = "{}mer_{}x{}_center{}x{}".format(
        kmer_size, left_bases, right_bases, args.first_center, args.last_center)

    split_conf = yaml.load(open(args.split_conf))
    plots_dir = "{}/detect_meth".format(split_conf["output_dir"])
    os.makedirs(plots_dir, exist_ok=True)

    data_loader = DataLoader(split_conf, crop_obs=0, reshape_for_keras=False)

    kmer_model = {}
    kmer_df = pd.read_csv(args.kmer_model)
    for _, row in kmer_df.iterrows():
        assert len(row["kmer"]) == kmer_size
        kmer_model[row["kmer"]] = (row["mean"], row["std"])

    pool = Pool()
    meth_types = [g for g in split_conf["groups"] if g.startswith("meth")]
    res = list(tqdm(pool.imap_unordered(compute_scores_for, meth_types),
                    total=len(meth_types)))

    # Merge all scores dfs and plot ROC curves.
    async_res = []
    for strand in ["+", "-", "both"]:
        if strand not in args.strands:
            continue

        scores_df = concatenate_dfs([v[strand] for v in res])

        dir_name = "{}/{}".format(plots_dir, model_name)
        os.makedirs(dir_name, exist_ok=True)

        fn_args = (strand_to_full_str(strand), scores_df, dir_name)
        async_res.append(pool.apply_async(eval_scores, fn_args))

    copyfile(args.kmer_model, "{}/{}/model.csv".format(plots_dir, model_name))

    for r in async_res:
        r.wait()
