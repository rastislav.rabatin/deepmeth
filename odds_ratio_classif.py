#!/usr/bin/env python3
import sys
import argparse

from scores_utils import *

from tqdm import tqdm
from multiprocessing import Pool


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Classify methylation patterns based on log odds ratio score for each 
        read and genome position.
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('epoch', type=int,
                        help='Number of the epoch which we want to visualize.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--med_filt_size', type=int, default=5,
                        help='Size of median filter kernel which is used for '
                             'smoothing the error signal.')
    parser.add_argument('--kmer_length', type=int, default=6,
                        help='Length of the k-mers which are used for '
                             'comparison.')
    parser.add_argument('--log_trans_err', action="store_true", default=False,
                        help='Apply log-transform to error.')
    return parser.parse_args()


def scores_for_strand(meth_id, train_data, dev_data):
    pcr_errs, meth_errs, pcr_bases_df, meth_bases_df = train_data
    predict_fn = fit_model_for(pcr_errs, meth_errs)

    pcr_errs, meth_errs, pcr_bases_df, meth_bases_df = dev_data

    df = get_scores_df(pcr_errs, meth_errs, pcr_bases_df, meth_bases_df,
                       meth_id, predict_fn)
    df["meth_id"] = meth_id

    return df


def compute_scores_for(meth_type):
    train_data = get_pcr_meth_errs_for("train", meth_type, split_conf,
                                       data_loader, model_name, args.epoch,
                                       args.med_filt_size, args.log_trans_err)
    dev_data = get_pcr_meth_errs_for("dev", meth_type, split_conf,
                                     data_loader, model_name, args.epoch,
                                     args.med_filt_size, args.log_trans_err)

    res = []
    for strand in ["+", "-"]:
        strand_train_data = select_data_for(strand, train_data)
        strand_dev_data = select_data_for(strand, dev_data)
        res.append(scores_for_strand(meth_type, strand_train_data,
                                     strand_dev_data))

    res.append(scores_for_strand(meth_type, train_data, dev_data))

    return res


if __name__ == "__main__":
    args = parse_args()
    pyplot_configs()
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)

    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)

    epoch_str = epoch_num_to_str(args.epoch)
    model_name = args.model_dir.split('/')[-1]
    plots_dir = "{}/odds_ratio/{}/{}".format(split_conf["output_dir"],
                                             model_name, epoch_str)
    os.makedirs(plots_dir, exist_ok=True)

    pool = Pool()
    meth_types = [g for g in split_conf["groups"] if g.startswith("meth")]
    res = list(tqdm(pool.imap_unordered(compute_scores_for, meth_types),
                    total=len(meth_types)))

    # Merge all scores dfs and plot ROC curves.
    async_res = []
    for idx, strand in enumerate(["fwd", "rev", "both"]):
        scores_df = concatenate_dfs([v[idx] for v in res])
        fn_args = (strand, scores_df, plots_dir)
        async_res.append(pool.apply_async(eval_scores, fn_args))

    for r in async_res:
        r.wait()
