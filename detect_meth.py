#!/usr/bin/env python3
import sys
import argparse

from err_models import *
from scores_utils import *
from utils import NoDeamonPool, strand_to_full_str

from tqdm import tqdm


def parse_args():
    parser = argparse.ArgumentParser(
        description='''
            Detect methylated windows.
        ''',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('epoch', type=int,
                        help='Number of the epoch which we want to visualize.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--med_filt_size', type=int, default=5,
                        help='Size of median filter kernel which is used for '
                             'smoothing the error signal.')
    parser.add_argument('--models', nargs="+", default=MODEL_NAMES,
                        help='Models of reconstruction error ({})'.format(
                            ", ".join(MODEL_NAMES)))
    parser.add_argument('--log_trans_err', action="store_true", default=False,
                        help='Apply log-transform to error.')
    parser.add_argument('--strands', nargs="+", default=["+", "-", "both"],
                        help='Strands for which to compute the models.')
    parser.add_argument('--test', action="store_true", default=False,
                        help='Run on testing set instead of development set.')
    return parser.parse_args()


def get_pcr_errs_for(dataset_type, meth_id, split_conf, data_loader,
                     model_name, epoch, med_filt_size):
    output_dir = split_conf["output_dir"]
    pcr_errs, pcr_bases_df = \
        get_errors_for(data_loader,
                       DatasetDesc(dataset_type, dataset_type, meth_id),
                       output_dir, model_name, epoch)

    pcr_errs = median_filter(pcr_errs, size=med_filt_size, mode='nearest')
    if args.log_trans_err:
        pcr_errs = np.log(pcr_errs)

    tb_stats_csv_path = split_conf["tombo_stats_csv"]
    pcr_bases_df = add_pattern_ref_pos(pcr_bases_df, meth_id, tb_stats_csv_path)

    return pcr_errs, pcr_bases_df


def select_train_data_for(strand, data):
    errs, bases_df = data
    strand = bases_df.strand == strand

    return errs[strand], bases_df[strand].reset_index(drop=True)


def compute_scores_for(meth_type):
    train_data = get_pcr_errs_for("train", meth_type, split_conf,
                                  data_loader, model_name, args.epoch,
                                  args.med_filt_size)

    dataset_type = "dev"
    if args.test:
        dataset_type = "test"

    dev_data = get_pcr_meth_errs_for(dataset_type, meth_type, split_conf,
                                     data_loader, model_name, args.epoch,
                                     args.med_filt_size, args.log_trans_err)

    res = {}
    for strand in ["+", "-"]:
        if strand not in args.strands:
            continue

        strand_train_data = select_train_data_for(strand, train_data)
        strand_dev_data = select_data_for(strand, dev_data)
        res[strand] = scores_for_strand(meth_type, strand_train_data,
                                        strand_dev_data, strand, plots_dir,
                                        args.models)

    if "both" in args.strands:
        res["both"] = scores_for_strand(meth_type, train_data, dev_data,
                                        "both", plots_dir, args.models)

    return res


if __name__ == "__main__":
    args = parse_args()

    for model in args.models:
        if model not in MODEL_NAMES:
            print("Model {} does not exists. Exiting...".format(model))
            sys.exit(0)

    pyplot_configs()
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)

    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)

    epoch_str = epoch_num_to_str(args.epoch)
    model_name = args.model_dir.split('/')[-1]
    plots_dir = "{}/detect_meth/{}/{}".format(split_conf["output_dir"],
                                              model_name, epoch_str)
    os.makedirs(plots_dir, exist_ok=True)

    pool = NoDeamonPool()
    meth_types = [g for g in split_conf["groups"] if g.startswith("meth")]
    res = list(tqdm(pool.imap_unordered(compute_scores_for, meth_types),
                    total=len(meth_types)))

    # Merge all scores dfs and plot ROC curves.
    async_res = []
    for strand in ["+", "-", "both"]:
        if strand not in args.strands:
            continue

        for model_name in MODEL_NAMES:
            if model_name not in args.models:
                continue

            scores_df = concatenate_dfs([v[strand][model_name] for v in res])

            dir_name = "{}/{}".format(plots_dir, model_name)
            if args.test:
                dir_name += "/test"
            os.makedirs(dir_name, exist_ok=True)

            fn_args = (strand_to_full_str(strand), scores_df, dir_name)
            async_res.append(pool.apply_async(eval_scores, fn_args))

    for r in async_res:
        r.wait()
