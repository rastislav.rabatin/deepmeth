#!/usr/bin/env python3
import argparse
import pandas as pd


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Print latex table from csv with rounded floats.
    ''')
    parser.add_argument('csv_path', type=str, help='Path to csv file.')
    parser.add_argument('--only_round_floats', action="store_true",
                        default=False,
                        help='Print csv file with rounded floats')
    parser.add_argument('--signif_places', type=int, default=2,
                        help='Number of significant places to round each '
                             'number.')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    df = pd.read_csv(args.csv_path)
    df = df.round(decimals=args.signif_places)

    if args.only_round_floats:
        print(df.to_csv(index=False))
    else:
        print(df.to_latex(index=False))
