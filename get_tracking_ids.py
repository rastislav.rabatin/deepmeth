""" Create TSV with tracking ids for every read/fast5 file.

Columns of the tsv are in COLS variable.
"""

#!/usr/bin/env python3
import h5py
import argparse
import logging
import sys

from multiprocessing import Pool


parser = argparse.ArgumentParser(
    description='Return tsv with tracking ids for every read.')
parser.add_argument('fast5_files_list', type=str,
                    help='File with list of fast5 files')
parser.add_argument('out_csv_filename', type=str,
                    help='Name of the output csv file.')
args = parser.parse_args()


DATA_DIR = '/extdata/nanoraw-data'

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

CONF_ID_ATTRS = ['uuid', 'read_id']
ATTRS = ['device_id', 'flow_cell_id', 'run_id', 'exp_script_name']
COLS = ['filename'] + ATTRS + CONF_ID_ATTRS


def get_tracking_ids(filename):
    TRACKING_ID_PATH = 'UniqueGlobalKey/tracking_id'
    CONF_ID_PATH = 'Analyses/Basecall_1D_000/Configuration/general'

    try:
        h5f = h5py.File(filename, 'r')
        tracking_ids = h5f[TRACKING_ID_PATH].attrs
        attr_vals = [str(tracking_ids[attr].decode("utf-8")) for attr in ATTRS]

        general = h5f[CONF_ID_PATH].attrs
        conf_ids = [str(general[attr].decode("utf-8")) for attr in CONF_ID_ATTRS]

        h5f.close()
        logging.info("Successfully parsed: %s", filename)
        return ",".join([filename] + attr_vals + conf_ids)
    except Exception as e:
        logging.info("Failed to parse: %s; Exception: %s", filename, e)
        return ""


files = [line.strip() for line in open(args.fast5_files_list, 'r')]
pool = Pool()
csv_lines = pool.map(get_tracking_ids, files)

with open(args.out_csv_filename, 'w+') as f:
    f.write(",".join(COLS) + "\n")
    for line in csv_lines:
        if line != '':
            f.write(line + "\n")
