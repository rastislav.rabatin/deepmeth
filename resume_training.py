#!/usr/bin/env python3
import argparse

from nn_utils import *


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Resume training of neural network.
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(filename=args.model_dir + "/training.log",
                        level=logging.DEBUG, filemode="a+")

    split_conf, train_conf = get_configs_from_model_dir(args)

    apply_tf_settings(train_conf["intra_op_threads"],
                      train_conf["inter_op_threads"])

    model, last_epoch = load_last_model(args.model_dir,
                                        train_conf["learning_rate"])

    model_module = __import__(train_conf["model_module"])
    train = getattr(model_module, 'train')

    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)
    train(model, train_conf, data_loader, args.model_dir, last_epoch)
