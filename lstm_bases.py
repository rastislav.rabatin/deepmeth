from keras.layers import *
from keras.models import Model

from nn_utils import *


def compile_model(train_conf, window_size):
    n_units = train_conf["n_units"]

    # Encoder
    encoder_inputs = Input(shape=(window_size,))
    x = Reshape(target_shape=(window_size, 1))(encoder_inputs)
    x = CuDNNLSTM(units=n_units, return_sequences=True)(x)
    x = CuDNNLSTM(units=n_units, return_sequences=True)(x)
    _, state_h, state_c = CuDNNLSTM(units=n_units, return_state=True)(x)

    # Decoder.
    n_crop_obs = train_conf["crop_obs"]
    output_size = window_size - 2*n_crop_obs
    decoder_inputs = Input(shape=(output_size, ALPHABET_SIZE))
    x = CuDNNLSTM(units=n_units, return_sequences=True)(
        decoder_inputs, initial_state=[state_h, state_c])
    x = CuDNNLSTM(units=n_units, return_sequences=True)(x)
    x = CuDNNLSTM(units=1, return_sequences=True)(x)
    predictions = Reshape(target_shape=(output_size,))(x)

    model = Model(inputs=[encoder_inputs, decoder_inputs], outputs=predictions)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]),
                  loss='mean_squared_error')

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    return prepare_bases_align(dataset_desc, data_loader, train_conf, model_dir)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data)
