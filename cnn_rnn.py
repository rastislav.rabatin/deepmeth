from keras.layers import *
from keras.models import Model

from nn_utils import *


def apply_conv_module(x, n_filters):
    x = Conv1D(filters=n_filters, kernel_size=8, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = BatchNormalization()(x)
    return Activation('elu')(x)


def compile_model(train_conf, window_size):
    add_bases = train_conf.get("add_bases", False)

    inputs = []
    sig = Input(shape=(window_size,))
    inputs.append(sig)

    bases = None
    if add_bases:
        kmer_size = 2 * train_conf["n_bases_around"] + 1
        bases = Input(shape=(kmer_size * ALPHABET_SIZE,))
        inputs.append(bases)

    x = sig
    x = Reshape(target_shape=(window_size, 1))(x)

    # Encoder convolutional modules.
    n_conv_modules = train_conf.get("n_conv_modules", 4)
    for _ in range(n_conv_modules):
        x = apply_conv_module(x, train_conf["filters"])
        x = AveragePooling1D(pool_size=2, strides=2, padding='same')(x)

    # Bottleneck layer.
    bottleneck_size = window_size//2**n_conv_modules
    bottleneck = CuDNNLSTM(units=bottleneck_size,
                           kernel_initializer='glorot_normal')(x)

    context = bottleneck
    if add_bases:
        context = Concatenate()([bottleneck, bases])

    x = RepeatVector(bottleneck_size)(context)
    x = CuDNNLSTM(units=bottleneck_size, kernel_initializer='glorot_normal',
                  return_sequences=True)(x)

    # Deconvolutional modules.
    for _ in range(n_conv_modules):
        x = apply_conv_module(x, train_conf["filters"])
        x = UpSampling1D(size=2)(x)

    # Last convolutional layer with only one filter. The shape of x should be
    # (512, train_conf["filters"]).
    x = Conv1D(filters=1, kernel_size=8, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = Activation('linear', name='linear')(x)

    n_crop_obs = train_conf["crop_obs"]
    x = Cropping1D(cropping=n_crop_obs)(x)
    predictions = Reshape((window_size - 2 * n_crop_obs,))(x)

    model = Model(inputs=inputs, outputs=predictions)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]),
                  loss='mean_squared_error')

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    if train_conf.get("add_bases", False):
        return prepare_signal_with_bases(dataset_desc, data_loader, train_conf,
                                         model_dir)
    else:
        return data_loader.get_signal_from(dataset_desc)


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data)
