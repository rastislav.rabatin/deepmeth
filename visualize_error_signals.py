#!/usr/bin/env python3
import sys
import tqdm
import argparse

import matplotlib.ticker as ticker

from plot_utils import *
from data_loader import *
from utils import strand_to_full_str

from functools import partial
from multiprocessing import Pool
from scipy.ndimage.filters import median_filter
from matplotlib.backends.backend_pdf import PdfPages


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Visualize reconstructions errors for a random subset of samples.
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('epoch', type=int,
                        help='Number of the epoch which we want to visualize.')
    parser.add_argument('--n_windows', type=int, default=2,
                        help='Number of random windows to visualize.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--med_filt_size', type=int, default=5,
                        help='Size of median filter kernel which is used for '
                             'smoothing the error signal.')
    return parser.parse_args()


def crop_windows(crop_obs, seg_starts, bases):
    crop_bases_begin = 0
    while crop_bases_begin < len(bases) and \
                    seg_starts[crop_bases_begin] < crop_obs:
        crop_bases_begin += 1

    crop_bases_end = len(seg_starts) - 1
    # Closed-open intervals.
    sig_win_end = split_conf["window_size"] - crop_obs
    while crop_bases_end > 0 and seg_starts[crop_bases_end] - 1 >= sig_win_end:
        crop_bases_end -= 1
    crop_bases_end += 1

    return (bases[crop_bases_begin:crop_bases_end],
            seg_starts[crop_bases_begin:crop_bases_end] - crop_obs)


def plot_window(signal, bases, seg_starts, label):
    plt.plot(signal, color='black', label=label)
    plt.xlim(0, len(signal))
    # We need to set these ticks even though we are going to hide them later.
    # We need them because of the grid.
    plt.xticks(seg_starts, bases)
    plt.grid(True)


# Not used.
def plot_running_difference(sqr_diff, bases, seg_starts):
    sqr_diff = np.cumsum(np.insert(sqr_diff, 0, 0))
    WIN_SIZE = 5
    running_diffs = np.abs((2 * sqr_diff[WIN_SIZE:-WIN_SIZE]) -
                           sqr_diff[:-2 * WIN_SIZE] - sqr_diff[2 * WIN_SIZE:])
    plot_window(running_diffs, bases, seg_starts, 'Running Differences')
    plt.ylabel('Running Differences')
    plt.ylim(0, 2)
    plt.legend(loc='upper right')
    plt.tick_params(bottom='off')


def plot_without_error(pdf, title, signal_true, signal_pred, bases, seg_starts):
    plt.rcParams["figure.figsize"] = (7, 2.5)

    fig = plt.figure()

    plot_window(signal_true, bases, seg_starts, 'Original')
    plt.plot(signal_pred, color='blue', label='Reconstructed')
    plt.ylabel('Signal')
    plt.ylim(-3, 3)
    plt.legend(loc='upper right')
    plt.tick_params(bottom='off')
    plt.title(title)

    # Major ticks are the visible ticks and minor ticks are the ticks for
    # nucleotide locations on x-axis.
    ax = fig.axes[-1]
    tick_loc = seg_starts
    middle = (tick_loc[:-1] + tick_loc[1:])/2
    ax.xaxis.set_major_locator(ticker.FixedLocator(tick_loc))
    ax.xaxis.set_major_formatter(ticker.NullFormatter())
    ax.xaxis.set_minor_locator(ticker.FixedLocator(middle))
    ax.xaxis.set_minor_formatter(ticker.FixedFormatter(bases))

    plt.xlabel('Reference Sequence')

    pdf.savefig(fig)
    plt.close()


def plot_error_windows(pdf, title, signal_true, signal_pred, bases, seg_starts):
    pyplot_configs()
    plt.rcParams["figure.figsize"] = (7, 5)

    fig = plt.figure()

    fig.add_subplot(211)
    plot_window(signal_true, bases, seg_starts, 'Original')
    plt.plot(signal_pred, color='blue', label='Reconstructed')
    plt.ylabel('Signal')
    plt.ylim(-3, 3)
    plt.legend(loc='upper right')
    plt.tick_params(bottom='off')
    plt.title(title)

    fig.add_subplot(212)
    sqr_diff = (signal_true - signal_pred) ** 2
    kernel_size = args.med_filt_size
    smooth_sqr_diff = median_filter(sqr_diff, size=kernel_size, mode='nearest')
    plot_window(smooth_sqr_diff, bases, seg_starts,
                "Kernel={}".format(kernel_size))
    plt.ylim(0, 1)
    plt.ylabel('Squared Difference')
    plt.legend(loc='upper right')
    plt.tick_params(bottom='on')

    fig.subplots_adjust(hspace=0.05)
    plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)

    # Major ticks are the visible ticks and minor ticks are the ticks for
    # nucleotide locations on x-axis.
    ax = fig.axes[-1]
    tick_loc = seg_starts
    middle = (tick_loc[:-1] + tick_loc[1:])/2
    ax.xaxis.set_major_locator(ticker.FixedLocator(tick_loc))
    ax.xaxis.set_major_formatter(ticker.NullFormatter())
    ax.xaxis.set_minor_locator(ticker.FixedLocator(middle))
    ax.xaxis.set_minor_formatter(ticker.FixedFormatter(bases))

    plt.xlabel('Reference Sequence')

    pdf.savefig(fig)
    plt.close()


def create_pdf_with_samples(dataset_desc, plots_dir):
    y_pred = get_predictions_for(dataset_desc, split_conf["output_dir"],
                                 model_name, args.epoch)

    _, y_true = data_loader.get_signal_from(dataset_desc)
    seg_starts = data_loader.get_seg_starts_from(dataset_desc)
    bases = data_loader.get_bases_from(dataset_desc)

    total_samples = y_pred.shape[0]
    subsample = np.random.choice(range(total_samples),
                                 min(args.n_windows, total_samples),
                                 replace=False)

    dataset_name = dataset_desc.meth_id
    if dataset_name is None:
        dataset_name = dataset_desc.name
    pdf_filename = "{}/{}".format(plots_dir, dataset_name)
    title = get_title(dataset_name)

    with PdfPages(pdf_filename + ".pdf") as pdf_with_errs,\
            PdfPages(pdf_filename + "_no_errs.pdf") as pdf_without_errs:
        for idx in subsample:
            # We need to crop base sequence and sequence of segment starts
            # because the sequences that we have contain bases for the input
            # windows. Not for the output windows which are smaller.
            cropped_bases, cropped_seg_starts = \
                crop_windows(n_crop_obs, seg_starts[idx], bases.base_seq[idx])

            strand = bases.strand[idx]

            plot_title = title + " - " + strand_to_full_str(strand)
            plot_error_windows(pdf_with_errs, plot_title,
                               y_true[idx, :], y_pred[idx, :], cropped_bases,
                               cropped_seg_starts)

            plot_without_error(pdf_without_errs, plot_title,
                               y_true[idx, :], y_pred[idx, :], cropped_bases,
                               cropped_seg_starts)


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(stream=sys.stdout,
                        level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)
    n_crop_obs = train_conf["crop_obs"]
    data_loader = DataLoader(split_conf, n_crop_obs, reshape_for_keras=False)

    dataset_names = [DatasetDesc(g, "dev", g)
                     for g in split_conf["groups"] if g.startswith("meth")]
    dataset_names += [DatasetDesc("dev", "dev", g)
                      for g in split_conf["groups"] if g.startswith("meth")]
    dataset_names.append(DatasetDesc("dev", "dev", None))

    epoch_str = epoch_num_to_str(args.epoch)
    model_name = args.model_dir.split('/')[-1]
    plots_dir = "{}/signal_plots/{}/{}".format(split_conf["output_dir"],
                                               model_name, epoch_str)
    os.makedirs(plots_dir, exist_ok=True)

    pool = Pool()
    create_pdf = partial(create_pdf_with_samples, plots_dir=plots_dir)
    res = list(tqdm.tqdm(pool.imap_unordered(create_pdf, dataset_names),
                         total=len(dataset_names)))
