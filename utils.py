import numpy as np
import multiprocessing as mp


def strand_to_full_str(strand):
    if strand == "+":
        return "fwd"
    elif strand == "-":
        return "rev"

    return "both"


class NoDaemonProcess(mp.Process):
    # make 'daemon' attribute always return False
    def _get_daemon(self):
        return False

    def _set_daemon(self, value):
        pass
    daemon = property(_get_daemon, _set_daemon)


# This pool allows to create a new process pool inside child process.
# Be careful, the number of processes can increase exponentially.
class NoDeamonPool(mp.pool.Pool):
    Process = NoDaemonProcess


def get_random_subsample(np_matrix, n_samples):
    total_samples = np_matrix.shape[0]
    indices = np.random.choice(total_samples, min(n_samples, total_samples),
                               replace=False)
    return np_matrix[indices, :]
