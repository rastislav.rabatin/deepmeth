#!/usr/bin/env python3
import h5py
import argparse
import sys
import logging
import os
import tqdm

import numpy as np

from multiprocessing import Pool


parser = argparse.ArgumentParser(description='''
Get statistics from fast5 groups created by tombo and dump them into csv.
''')
parser.add_argument('fast5_files_list', type=str,
                    help='File with the list of fast5 files.')
parser.add_argument('out_csv_filename', type=str,
                    help='Name of the output csv file.')
parser.add_argument('--tombo_group', type=str,
                    default='/Analyses/TomboEventResquiggle_bwa-mem/'
                            'BaseCalled_template',
                    help='HDF5 group which was created by tombo.')
parser.add_argument(
    '--tombo_eventless_resquiggle_group', type=str,
    default='/Analyses/TomboResquiggle_bwa-mem/BaseCalled_template/Alignment',
    help="HDF5 group created by tombo eventless resquiggle."
         "This group should contain atttributes with alignment statistics."
         "Tombo eventless resquiggle uses the true basecall - without skipping"
         "moves > 1."
)
args = parser.parse_args()


logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


COLS = [
    'read_id',
    'meth_group',
    'library',
    'lower_lim_signal',
    'upper_lim_signal',
    'scale',
    'shift',
    'outlier_threshold',
    'clipped_bases_start',
    'clipped_bases_end',
    'clipped_bases_total',
    # 0-based index.
    'ref_start',
    'ref_end',
    'strand',
    'n_del',
    'n_ins',
    'n_sub',
    'n_match',
    'identity',
    # 0-based index of the start of the read in the raw signal.
    'read_start_rel_to_raw',
    # Length of the read in the number of signal samples.
    'total_seg_len_sum',
    # Segment is a segment of signal which should correspond to one base.
    'n_segments',
    'seg_obs_mean',
    'seg_obs_std',
    'seg_obs_min',
    'seg_obs_max',
    'seg_obs_Q1',
    'seg_obs_median',
    'seg_obs_Q3',
    'seg_obs_Q95',
    # Number of bases in the alignment after removing gaps.
    'aligned_bases_from_ref',
    'aligned_bases_from_read',
    'basecall_len',
    'basecall_len_no_clips',
    # Original events which were used for basecalling.
    'events_obs_mean',
    'events_obs_min',
    'events_obs_max',
    'events_obs_Q1',
    'events_obs_median',
    'events_obs_Q3',
    'events_obs_Q95',
    'n_events',
    'n_del_eventless',
    'n_ins_eventless',
    'n_sub_eventless',
    'n_match_eventless',
    'identity_eventless',
    'clipped_bases_start_eventless',
    'clipped_bases_end_eventless',
]


def dataset_to_str(dataset):
    return "".join(c.decode("utf-8") for c in dataset)


def extract_stats(filename):
    try:
        h5f = h5py.File(filename, 'r')
        tb_group = h5f[args.tombo_group]

        split_path = filename.split('/')
        norm = tb_group.attrs

        aln = tb_group['Alignment'].attrs
        n_del = aln['num_deletions']
        n_ins = aln['num_insertions']
        n_sub = aln['num_mismatches']
        n_match = aln['num_matches']
        clipped_bases_start = aln['clipped_bases_start']
        clipped_bases_end = aln['clipped_bases_end']

        events = tb_group['Events']
        seg_lengths = events.value['length']

        ref_alignment = dataset_to_str(
            tb_group['Alignment/genome_alignment']).replace('-', '')
        read_alignment = dataset_to_str(
            tb_group['Alignment/read_alignment']).replace('-', '')

        fastq_path = '/Analyses/Basecall_1D_000/BaseCalled_template/Fastq'
        fastq = str(h5f[fastq_path].value.decode("utf-8"))
        basecall = fastq.split('\n')[1]

        events_path = '/Analyses/Basecall_1D_000/BaseCalled_template/Events'
        sampling_rate = h5f['UniqueGlobalKey/channel_id'].attrs['sampling_rate']
        events_lengths = h5f[events_path].value['length'] * sampling_rate

        eventless_resquiggle_aln_path = \
            '/Analyses/TomboResquiggle_bwa-mem/BaseCalled_template/Alignment'
        n_del_eventless = -1
        n_ins_eventless = -1
        n_sub_eventless = -1
        n_match_eventless = -1
        indentity_eventless = -1
        clipped_bases_start_eventless = -1
        clipped_bases_end_eventless = -1
        if h5f.get(eventless_resquiggle_aln_path, default=None) is not None:
            eventless_aln = h5f[eventless_resquiggle_aln_path].attrs
            n_del_eventless = eventless_aln['num_deletions']
            n_ins_eventless = eventless_aln['num_insertions']
            n_sub_eventless = eventless_aln['num_mismatches']
            n_match_eventless = eventless_aln['num_matches']
            clipped_bases_start_eventless = eventless_aln['clipped_bases_start']
            clipped_bases_end_eventless = eventless_aln['clipped_bases_end']

            aln_len = n_ins_eventless+n_del_eventless+n_sub_eventless+n_match_eventless
            indentity_eventless = (n_match_eventless/aln_len)*100

        res = [
            os.path.splitext(split_path[-1])[0],  # filename = read_id
            split_path[-2],  # methXX or controlX
            split_path[-3],  # lib1, lib2 or lib3
            norm['lower_lim'],
            norm['upper_lim'],
            norm['scale'],
            norm['shift'],
            norm['outlier_threshold'],
            clipped_bases_start,
            clipped_bases_end,
            clipped_bases_start + clipped_bases_end,
            aln['mapped_start'],
            aln['mapped_end'],
            aln['mapped_strand'].decode("utf-8"),
            n_del,
            n_ins,
            n_sub,
            n_match,
            n_match/(n_ins+n_del+n_sub+n_match)*100,
            events.attrs['read_start_rel_to_raw'],
            seg_lengths.sum(),
            seg_lengths.shape[0],
            seg_lengths.mean(),
            seg_lengths.std(),
            seg_lengths.min(),
            seg_lengths.max(),
            np.percentile(seg_lengths, 25),
            np.median(seg_lengths),
            np.percentile(seg_lengths, 75),
            np.percentile(seg_lengths, 95),
            len(ref_alignment),
            len(read_alignment),
            len(basecall),
            len(basecall) - clipped_bases_end - clipped_bases_start,
            np.mean(events_lengths),
            events_lengths.min(),
            events_lengths.max(),
            np.percentile(events_lengths, 25),
            np.median(events_lengths),
            np.percentile(events_lengths, 75),
            np.percentile(events_lengths, 95),
            len(events_lengths),
            n_del_eventless,
            n_ins_eventless,
            n_sub_eventless,
            n_match_eventless,
            indentity_eventless,
            clipped_bases_start_eventless,
            clipped_bases_end_eventless,
        ]

        h5f.close()

        return ",".join(map(str, res))
    except Exception as e:
        logging.info("Failed to parse: %s; Exception: %s", filename, e)
        if h5f is not None:
            h5f.close()

        return ''


files = [line.strip() for line in open(args.fast5_files_list, 'r')]
pool = Pool()
csv_lines = []
for line in tqdm.tqdm(pool.imap_unordered(extract_stats, files),
                      total=len(files), file=sys.stdout):
    csv_lines.append(line)

# Create csv file.
with open(args.out_csv_filename, 'w+') as f:
    f.write(",".join(COLS) + "\n")
    for line in csv_lines:
        if line != '':
            f.write(line + "\n")
