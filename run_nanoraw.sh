#!/bin/bash
# Runs nanoraw resquiggle. Requires installed bwa-mem or graphmap.

if [ "$#" -ne 3 ]; then
    echo "Usage:"
    echo "./run_nanoraw fast5_dir [bwa|graphmap] n_jobs directory_indetifier"
    exit
fi

ref_seq="/extdata/nanoraw-data/ref_seq/U96.3.fasta"
fast5_dir=$1
aligner=$2
n_jobs=$3
dir_id=$4

exec_flag="bwa-mem"
if [ $aligner == "graphmap" ]; then
    exec_flag=$aligner
fi

nanoraw genome_resquiggle \
      $fast5_dir $ref_seq --${exec_flag}-executable $aligner \
      --normalization-type median \
      --corrected-group "RawGenomeCorrected_${aligner}_000" --overwrite \
      --failed-reads-filename $fast5_dir/../../nanoraw/nanoraw_failed_${dir_id}_${aligner}.list \
      --2d --processes $n_jobs
