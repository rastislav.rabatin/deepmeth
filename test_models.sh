#!/usr/bin/env bash

set -o xtrace
set -e

split_conf="/scratch/data_split3/split_conf.yaml"

run () {
    logs_dir=$(dirname ${split_conf})/logs/$(basename ${model_dir})
    mkdir -p $logs_dir
    logs_file=${logs_dir}/${epoch}

    time detect_meth.py $model_dir $epoch --split_conf $split_conf \
        --strands + both \
        --models mse_simple mse_middle diag_norm inv_gauss inv_gauss_middle \
        --test >${logs_file}_meth_detect_test.stdout
}

model_dir="mlp16_20180331-165604"; epoch=1989
run
# Bottleneck 32
model_dir="mlp_bases_20180319-161642"; epoch=4272
run
# Bottleneck 64
model_dir="mlp_bases_20180331-165151"; epoch=2686
run

model_dir="autoencoder_bottleneck16_20180301-145018"; epoch=2736
run
model_dir="autoencoder_bottleneck32_20180124-004356"; epoch=2079
run
model_dir="neck64_no_bases_20180320-202205"; epoch=1809
run

model_dir="bases32_9mer_20180427-220734"; epoch=569
run
model_dir="bases32_10mer_20180430-000203"; epoch=226;
run
model_dir="bases32_20180321-143419"; epoch=2292;
run

model_dir="lstm_bases32_20180425-090929"; epoch=199
run

model_dir="bases_neck32_no_neck_bases_20180311-203418"; epoch=2082
run
model_dir="cnn_rnn32_20180331-132520"; epoch=2715;
run
model_dir="cnn_rnn64_20180416-012117"; epoch=459
run
model_dir="cnn_rnn64_20180420-005253"; epoch=1163
run
