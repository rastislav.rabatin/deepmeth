#!/bin/bash

if [ "$#" -ne 6 ]; then
    echo "Usage:"
    echo "./tb_resquiggle.sh [bwa-mem|graphmap|minimap2] \\"
    echo "  failed_reads_filename n_jobs fast5_basedir ref_fasta \\"
    echo "  tombo_model"
    exit
fi

aligner=$1
failed_reads_filename=$2
n_jobs=$3
fast5_basedir=$4
ref_fasta=$5
tb_model=$6

tombo resquiggle \
    --${aligner}-executable $aligner \
    --tombo-model-filename $tb_model \
    --failed-reads-filename $failed_reads_filename \
    --include-event-stdev \
    --processes $n_jobs \
    --corrected-group "TomboResquiggle_${aligner}"\
    --overwrite \
    $fast5_basedir $ref_fasta
