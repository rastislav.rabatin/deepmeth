import signal
import h5py

from data_loader import *

from functools import partial
from termcolor import colored

from keras.callbacks import *
from keras.utils import plot_model
from keras.optimizers import Adam
from keras.models import model_from_yaml, load_model, Sequential
from keras.utils import to_categorical

ALPHABET_SIZE = 4
BASE2NUM = {'A': 0, 'C': 1, 'T': 2, 'G': 3}


class EpochTime(Callback):
    def __init__(self):
        super(EpochTime, self).__init__()
        self.epoch_time_start = time.time()

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        cur_time = time.localtime()
        logs["epoch_start"] = time.strftime(
            "%Y-%m-%d-%H-%M-%S", time.localtime(self.epoch_time_start))
        logs["epoch_end"] = time.strftime("%Y-%m-%d-%H-%M-%S", cur_time)
        logs["epoch_time"] = time.mktime(cur_time) - self.epoch_time_start


class MethylationTester(Callback):
    def __init__(self, meth_sets, batch_size):
        """
        Attributes:
            meth_sets: List of (X, y, meth_group_name).
            batch_size (int): Batch size used during evaluation.
        """
        super(MethylationTester, self).__init__()
        self.batch_size = batch_size
        self.meth_sets = meth_sets

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        for dataset_name, (X, y) in self.meth_sets.items():
            results = self.model.evaluate(x=X, y=y, verbose=True,
                                          batch_size=self.batch_size)

            if self.model.metrics is None:
                results = [results]

            for i, result in enumerate(results):
                if i == 0:
                    name = dataset_name + '_loss'
                else:
                    name = dataset_name + '_' + self.model.metrics[
                        i - 1].__name__

                logs[name] = result


# TODO refactor this part.
class SignalCallback(Callback):
    def __init__(self, lr_scheduler, train_conf_path):
        super(SignalCallback, self).__init__()
        self.train_conf_path = train_conf_path
        self.lr_scheduler = lr_scheduler
        self.caught_sig_int = False
        self.caught_sig_hup = False
        self.logger = logging.getLogger(__name__)

        signal.signal(signal.SIGINT, self.sig_int)
        signal.signal(signal.SIGHUP, self.sig_hup)

    def sig_int(self, signum, _):
        if signum != signal.SIGINT:
            return

        msg = 'SIGINT: The training is going to stop after this epoch.'
        self.logger.info(msg)
        print(colored(msg, 'red'), flush=True)
        self.caught_sig_int = True

    def sig_hup(self, signum, _):
        if signum != signal.SIGHUP:
            return

        msg = 'SIGHUP: Reloading learning rate schedule from ' \
              + self.train_conf_path
        self.logger.info(msg)
        print(colored(msg, 'red'), flush=True)
        self.caught_sig_hup = True

    def on_epoch_end(self, epoch, logs=None):
        if self.caught_sig_int:
            self.model.stop_training = True
            self.logger.info("Epoch %d: Training manually stopped.", epoch + 1)

        if self.caught_sig_hup:
            self.logger.info("Epoch %d: Reloading learning rate schedule "
                             "from %s ...", epoch + 1, self.train_conf_path)
            new_schedule, schedule_conf = \
                load_lr_schedule(yaml.load(open(self.train_conf_path)))
            if schedule_conf is None:
                self.logger.info("Epoch %d: No schedule found in %s", epoch + 1,
                                 self.train_conf_path)
            else:
                self.lr_scheduler.schedule = new_schedule
                schedule_str = schedule_conf["function_name"] + \
                               str(schedule_conf["args"])
                self.logger.info("Epoch %d: New schedule %s", epoch + 1,
                                 schedule_str)

            self.caught_sig_hup = False


def get_checkpointer(model_dir):
    os.makedirs(model_dir + "/checkpoints", exist_ok=True)
    filepath = model_dir + "/checkpoints/epoch_{epoch:05d}_loss_{loss:.4f}_" \
                           "val_loss_{val_loss:.4f}.hdf5"

    return ModelCheckpoint(verbose=True, monitor="val_loss", filepath=filepath)


def load_lr_schedule(train_conf):
    if "lr_schedule" not in train_conf:
        return (lambda _: train_conf["learning_rate"]), None

    schedule_conf = train_conf["lr_schedule"]
    lr_schedule = partial(globals()[schedule_conf["function_name"]],
                          **schedule_conf["args"])

    return lr_schedule, schedule_conf


def load_model_from_weights(model_dir, epoch, learning_rate):
    """ Load model from weights.

    Model structure is stored in yaml file and model weights are stored in
    hdf5 file. Unfortunately, optimizers weights are not stored.
    """
    logger = logging.getLogger(__name__)

    with open(model_dir + "/model.yaml") as f:
        model = model_from_yaml("".join(f.readlines()))

    weights_dir = model_dir + "/weights"
    path = weights_dir + "/" + get_filename_with(epoch, weights_dir)
    logger.info("Loading weights from %s", path)
    model.load_weights(path)

    model.compile(optimizer=Adam(lr=learning_rate),
                  loss='mean_squared_error')

    return model


def load_model_from_checkpoint(model_dir, epoch):
    checkpoints_dir = model_dir + "/checkpoints"
    path = checkpoints_dir + "/" + get_filename_with(epoch, checkpoints_dir)

    logger = logging.getLogger(__name__)
    logger.info("Loading checkpoint from %s", path)

    try:
        model = load_model(path)
    except ValueError as e:
        # VAE error because of loss function.
        train_conf = yaml.load(open(model_dir + "/train_conf.yaml"))
        split_conf = yaml.load(open(model_dir + "/split_conf.yaml"))
        win_size = split_conf["window_size"]

        model_module = __import__(train_conf["model_module"])
        compile_model = getattr(model_module, 'compile_model')
        model = compile_model(train_conf, win_size)
        model.load_weights(path)

        if isinstance(model, Sequential):
            model.model._make_train_function()
        else:
            model._make_train_function()

        # Load optimizer weights separately.
        with h5py.File(path, mode='r') as f:
            optimizer_weight_names = [
                n.decode('utf8')
                for n in f['optimizer_weights'].attrs['weight_names']
            ]
            optimizer_weight_values = [f['optimizer_weights'][n] for n in
                                       optimizer_weight_names]
            model.optimizer.set_weights(optimizer_weight_values)

    return model


def load_last_model(model_dir, learning_rate=0.001):
    # Weights files contain only model weights. Checkpoint files also contain
    # network structure and optimizers weights.
    # Support loading weights because of compatibility. The new code saves
    # everything including structure to one file.
    checkpoints_dir = model_dir + "/checkpoints"
    weights_dir = model_dir + "/weights"

    if os.path.isdir(checkpoints_dir):
        epoch = get_last_epoch(checkpoints_dir)
        return load_model_from_checkpoint(model_dir, epoch), epoch

    epoch = get_last_epoch(weights_dir)
    return load_model_from_weights(model_dir, epoch, learning_rate), epoch


def load_model_from(model_dir, epoch, learning_rate):
    if epoch_exists_in(model_dir + "/checkpoints", epoch):
        return load_model_from_checkpoint(model_dir, epoch)

    return load_model_from_weights(model_dir, epoch, learning_rate)


def save_model_structure(model, output_dir):
    model_yaml = model.to_yaml()
    with open(output_dir + "/model.yaml", "w+") as yaml_file:
        yaml_file.write(model_yaml)

    try:
        plot_model(model, output_dir + "/model.png", show_shapes=True)
    except ImportError:
        msg = "Failed to create graph of the model: pydot and/or graphviz is " \
              "missing"
        logging.getLogger(__name__).warning(msg)
        print(msg, flush=True)


def save_model_summary(model, output_dir):
    with open(output_dir + "/model_summary.txt", "w+") as f:
        model.summary(print_fn=lambda line: f.write(line + "\n"))


# The version of keras on master can log the change in lr. The
# current pip version cannot do this (Jan 11th 2018).
def log_learning_rate(fn):
    def wrapper(*args, **kwargs):
        new_lr, epoch = fn(*args, **kwargs)
        logging.getLogger(__name__).info("Epoch %d: Setting learning rate to "
                                         "%f", epoch + 1, new_lr)

        return new_lr

    return wrapper


@log_learning_rate
def keras_lr_decay(epoch, initial_lr, start_decay_epoch, decay):
    if epoch <= start_decay_epoch:
        return initial_lr, epoch

    return initial_lr / (1 + decay * (epoch - start_decay_epoch)), epoch


def get_only_meth(dataset_to_val):
    return {dataset_name: val
            for dataset_name, val in dataset_to_val.items()
            if dataset_name.startswith("meth")}


def apply_tf_settings(intra_op_threads, inter_op_threads):
    if K.backend() == "tensorflow":
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session

        session_conf = tf.ConfigProto(
            intra_op_parallelism_threads=intra_op_threads,
            inter_op_parallelism_threads=inter_op_threads)
        session_conf.gpu_options.allow_growth=True
        set_session(tf.Session(config=session_conf))


def get_kmer_size_from_config(train_conf):
    # Support also the old option "n_bases_around". When we specify the number
    # of bases to the left and to the right then it is more extensible.
    n_bases_around = train_conf.get("n_bases_around", None)
    n_bases_left = train_conf.get("n_bases_left", None)
    n_bases_right = train_conf.get("n_bases_right", None)
    if n_bases_around is not None:
        n_bases_left = n_bases_around
        n_bases_right = n_bases_around

    kmer_size = n_bases_left + n_bases_right + 1

    return kmer_size, n_bases_left, n_bases_right


def prepare_signal_with_bases(dataset_desc, data_loader, train_conf, model_dir):
    sig = data_loader.get_signal_from(dataset_desc)
    base_seqs = data_loader.get_bases_from(dataset_desc).base_seq.values
    seg_starts = data_loader.get_seg_starts_from(dataset_desc)

    win_size = sig[0].shape[1]
    middle = win_size // 2

    skip_sample = filter_windows(seg_starts, win_size)
    seg_starts = [seg for seg, skip in zip(seg_starts, skip_sample) if not skip]
    base_seqs = base_seqs[~skip_sample]
    X = sig[0][~skip_sample]
    y = sig[1][~skip_sample]

    kmer_size, n_bases_left, n_bases_right = \
        get_kmer_size_from_config(train_conf)

    n_samples = X.shape[0]
    kmers = np.zeros((n_samples, kmer_size * ALPHABET_SIZE))
    for pos, (starts, seq) in enumerate(zip(seg_starts, base_seqs)):
        mid_base = np.searchsorted(starts, middle)

        # Binary search found the next base.
        # The middle signal is in between two starts.
        if starts[mid_base] != middle:
            mid_base -= 1

        kmer = seq[mid_base - n_bases_left:mid_base + n_bases_right + 1]
        one_hot_kmer = to_categorical([BASE2NUM[base] for base in kmer],
                                      num_classes=ALPHABET_SIZE)
        kmers[pos, :] = one_hot_kmer.flatten()

    inputs = [X, kmers]
    outputs = y

    assert inputs[0].shape[0] == inputs[0].shape[0]
    assert outputs.shape[0] == inputs[0].shape[0]

    logger = logging.getLogger(__name__)

    n_samples = len(skip_sample)
    n_skip = np.sum(skip_sample)
    logger.warning("Skipped %d (%f %% -- %d) samples for %s.", n_skip,
                   (n_skip / n_samples) * 100, n_samples, dataset_desc)

    filename = "{}/skipped_samples_{}.txt".format(
        model_dir, dataset_desc_to_str(dataset_desc))
    np.savetxt(filename, np.where(skip_sample), delimiter='\n', fmt="%d")

    return inputs, outputs


def prepare_bases_align(dataset_desc, data_loader, train_conf, model_dir):
    logger = logging.getLogger(__name__)

    sig = data_loader.get_signal_from(dataset_desc)
    base_seqs = data_loader.get_bases_from(dataset_desc).base_seq.values
    seg_starts = data_loader.get_seg_starts_from(dataset_desc)

    logger.info("Running the 1st phase of filtering for %s", dataset_desc)
    n_samples, win_size = sig[0].shape
    skip_sample = filter_windows(seg_starts, win_size)
    logger.info("1st phase of filtering is finished for %s", dataset_desc)

    n_crop_obs = train_conf["crop_obs"]
    seqs = np.zeros((n_samples, win_size - 2*n_crop_obs, ALPHABET_SIZE))
    logger.info("Unpacking the base sequences for %s", dataset_desc)
    for pos, (starts, seq) in enumerate(zip(seg_starts, base_seqs)):
        if skip_sample[pos]:
            continue

        bases_aligned = unpack_bases(seq, starts, win_size)
        bases_aligned = bases_aligned[n_crop_obs:-n_crop_obs]

        if bases_aligned[0] == "-":
            skip_sample[pos] = True
            continue

        subseq = [BASE2NUM[base] for base in bases_aligned]
        seqs[pos, :] = to_categorical(subseq, num_classes=ALPHABET_SIZE)
    logger.info("Unpacking of the base sequences for %s is finished.",
                dataset_desc)

    inputs = [sig[0][~skip_sample, :], seqs[~skip_sample]]
    outputs = sig[1][~skip_sample, :]

    assert inputs[0].shape[0] == inputs[0].shape[0]

    n_samples = len(skip_sample)
    n_skip = np.sum(skip_sample)
    logger.info("Skipped %d (%f %% -- %d) samples for %s.", n_skip,
                (n_skip / n_samples) * 100, n_samples, dataset_desc)

    filename = "{}/skipped_samples_{}.txt".format(
        model_dir, dataset_desc_to_str(dataset_desc))
    np.savetxt(filename, np.where(skip_sample), delimiter='\n', fmt="%d")

    return inputs, outputs


def train_without_meth_val(model, train_conf, data_loader, model_dir,
                           initial_epoch, prepare_data_fn,
                           additional_callbacks=[]):
    train_data = prepare_data_fn(DatasetDesc("train", "train", None),
                                 data_loader, train_conf, model_dir)
    dev_data = prepare_data_fn(DatasetDesc("dev", "dev", None), data_loader,
                               train_conf, model_dir)

    schedule, _ = load_lr_schedule(train_conf)
    lr_scheduler = LearningRateScheduler(schedule=schedule)
    keras_callbacks = [
        get_checkpointer(model_dir),
        EpochTime(),
        CSVLogger(model_dir + "/epochs.csv", append=True),
        SignalCallback(lr_scheduler, model_dir + "/train_conf.yaml"),
        lr_scheduler
    ] + additional_callbacks

    return model.fit(train_data[0], train_data[1],
                     initial_epoch=initial_epoch,
                     epochs=train_conf["n_epochs"],
                     verbose=True, batch_size=train_conf["batch_size"],
                     validation_data=dev_data, shuffle=True,
                     callbacks=keras_callbacks)


def train_with_meth_val(model, train_conf, data_loader, model_dir,
                       initial_epoch):
    datasets = data_loader.get_all_signal_data()

    schedule, _ = load_lr_schedule(train_conf)
    lr_scheduler = LearningRateScheduler(schedule=schedule)
    keras_callbacks = [
        MethylationTester(get_only_meth(datasets), train_conf["batch_size"]),
        get_checkpointer(model_dir),
        EpochTime(),
        CSVLogger(model_dir + "/epochs.csv", append=True),
        SignalCallback(lr_scheduler, model_dir + "/train_conf.yaml"),
        lr_scheduler,
    ]

    return model.fit(datasets["train"][0], datasets["train"][1],
                     initial_epoch=initial_epoch,
                     epochs=train_conf["n_epochs"],
                     verbose=True, batch_size=train_conf["batch_size"],
                     validation_data=datasets["dev"], shuffle=True,
                     callbacks=keras_callbacks)
