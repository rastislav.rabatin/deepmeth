#!/usr/bin/env python3
import h5py
import argparse
import sys
import logging
import tqdm
import yaml
import random
import os

import pandas as pd
import numpy as np

from shutil import copyfile
from functools import partial
from multiprocessing import Pool

from window_iterators import gen_control_window, gen_meth_window
from data_loader import get_dataset_path_suffix, DatasetDesc

parser = argparse.ArgumentParser(description='''
    Create train/dev/test data for every control set and dev/test for every
    methylation group.
''')
parser.add_argument('-c', '--yaml_config', type=str, default="split_conf.yaml",
                    help='Yaml config file.')
parser.add_argument('--read_split', action='store_true', default=False,
                    help='Create read split instead of window split.')
args = parser.parse_args()

conf = yaml.load(open(args.yaml_config))
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

FLOAT_TYPE = np.float32

# Forward strand methylation patterns.
# For all of the pattern this holds true:
# base[i] == complement(base[-i])
# Legend:
# Y = C or T (pyrimidine)
# R = A or G (purine)
# W = weak A or T
METH_PATTERNS = {
    "meth01": "TCGA",
    "meth02": "GGATCC",
    "meth03": "GGCGCC",
    "meth04": "GAATTC",
    "meth05": "TCTAGA",
    "meth06": "WCCGGW",
    "meth07": "GTYRAC",
    "meth08": "GCGC",
    "meth09": "CG",
    "meth10": "CG",
    "meth11": "GATC",
    "meth12": "GRCGYC",
}


def list2csv(l):
    return ",".join(str(e) for e in l)


def get_base_seg(filename, row):
    """Extract signal base segmentation from fast5.

    Returns:
         signal (np.array): Normalized and winsorized signal.
         seg_starts (np.array): Starts of the segments. Each segment
             corresponds to one base. First segments starts at time zero.
         base_seq (str): Sequence of bases corresponding to the segments.
    """
    read_start = row['read_start_rel_to_raw']
    total_obs = row['total_seg_len_sum']
    read_end = read_start + total_obs

    try:
        h5f = h5py.File(filename, 'r')

        reads = list(h5f["/Raw/Reads"].values())

        # We always have only one read.
        assert len(reads) == 1
        raw_signal = reads[0]['Signal'].value[read_start:read_end]

        segments = h5f[conf['base_seg_group']].value
        bases = "".join(base.decode("utf-8") for base in segments['base'])
        seg_starts = segments['start']
        seg_lengths = segments['length']

        h5f.close()
    except Exception as e:
        if h5f is not None:
            h5f.close()

        raise e

    assert seg_starts[-1] + seg_lengths[-1] == total_obs

    # MAD normalization.
    shift = row['shift']
    scale = row['scale']
    raw_signal = (raw_signal - shift) / scale

    # Winsorization.
    lower_lim = row['lower_lim_signal']
    upper_lim = row['upper_lim_signal']
    raw_signal[raw_signal < lower_lim] = lower_lim
    raw_signal[raw_signal > upper_lim] = upper_lim

    return raw_signal, seg_starts, bases


def get_dataset_for_read(row):
    """Put the read into to the right dataset based on the position on the
    reference to which the read was mapped.
    """
    ref_start = row['ref_start']
    ref_end = row['ref_end']
    if ref_start >= conf['train_start'] and ref_end < conf['train_end']:
        return 'train'
    elif ref_start >= conf['train_end'] and ref_end < conf['dev_end']:
        return 'dev'
    elif ref_start >= conf['dev_end'] and ref_end < conf['test_end']:
        return 'test'

    return None


class ReadReservoir:
    def __init__(self, dataset_desc: DatasetDesc):
        self.n_samples = 0
        self.total_bases = 0
        self.dataset_desc = dataset_desc

    def add(self, signal, seg_starts, base_seq, tb_csv_row):
        path = conf['output_dir'] + get_dataset_path_suffix(self.dataset_desc)
        os.makedirs(path, exist_ok=True)

        path += "/{}".format(tb_csv_row["read_id"])
        np.savez_compressed(path + "_signal.npz", signal)
        np.savez_compressed(path + "_seg_starts.npz", seg_starts)

        csv_lines = [
            ["base_seq", base_seq],
            ["strand", tb_csv_row["strand"]],
            ["base_len", len(base_seq)],
            ["ref_start", tb_csv_row["ref_start"]],
            ["ref_end", tb_csv_row["ref_end"]]
        ]

        with open(path + "_bases.csv", "w+") as f:
            for line in csv_lines:
                f.write(list2csv(line) + "\n")

        self.n_samples += 1
        self.total_bases += len(base_seq)

    def get_stats_line(self):
        dataset_desc = self.dataset_desc
        return list2csv([dataset_desc.name, dataset_desc.type,
                         self.n_samples,
                         self.total_bases, str(dataset_desc.meth_id)])


# Reservoir sampling.
class Reservoir:
    def __init__(self, dataset: str):
        self.csv_lines = []
        self.n_samples = 0
        self.total_bases = 0
        self.CSV_HEADER = ["read_id", "base_seq", "bases_start", "bases_end",
                           "sig_start", "sig_end", "strand", "base_len"]
        capacity = conf[dataset + "_samples"]
        self.signals = np.zeros((capacity, conf['window_size']),
                                dtype=FLOAT_TYPE)
        self.seg_starts = []

    def _add(self, pos, win_signal, csv_line, seg_starts):
        assert len(csv_line) == len(self.CSV_HEADER)

        self.signals[pos, :] = win_signal

        if pos >= len(self.csv_lines):
            self.csv_lines.append(list2csv(csv_line))
            self.seg_starts.append(seg_starts)
        else:
            self.csv_lines[pos] = list2csv(csv_line)
            self.seg_starts[pos] = seg_starts

    def add(self, win_signal, csv_line, seg_starts):
        n_bases = len(csv_line[1])
        self.total_bases += n_bases
        csv_line.append(n_bases)

        if self.total_samples() < self.capacity():
            self._add(self.total_samples(), win_signal, csv_line, seg_starts)
            self.n_samples += 1
            return

        j = random.randint(0, self.total_samples())
        if j < self.capacity():
            self._add(j, win_signal, csv_line, seg_starts)

        self.n_samples += 1

    def total_samples(self):
        return self.n_samples

    def capacity(self):
        return self.signals.shape[0]

    def save(self, dataset_desc):
        path = conf['output_dir'] + get_dataset_path_suffix(dataset_desc)
        os.makedirs(path, exist_ok=True)

        signals = self.signals
        if self.total_samples() < self.capacity():
            signals = signals[:self.total_samples(), :]
        np.savez_compressed(path + "/signal.npz", signals)

        np.savez_compressed(path + "/seg_starts.npz", self.seg_starts)

        with open(path + "/bases.csv", "w+") as f:
            f.write("\n".join([list2csv(self.CSV_HEADER)] + self.csv_lines))
            f.write("\n")

        return list2csv([dataset_desc.name, dataset_desc.type, self.n_samples,
                         self.total_bases, str(dataset_desc.meth_id)])


def split_dataset(group_data):
    """
    meth_group - control1, control2, meth01, meth02, ..., meth12
               - the dataset from which the data comes from.
    dataset - train, dev, test
    df - pd.DataFrame with reads from tombo stats df
    meth_id - None, meth01, meth02, ..., meth12
            - methylation pattern that we want to extract from the samples
            in this dataset.
            - None means that we don't want to extract any meth pattern.
    """
    meth_group, dataset, df, meth_id = group_data

    dataset_desc = DatasetDesc(meth_group, dataset, meth_id)
    if args.read_split:
        reservoir = ReadReservoir(dataset_desc)
    else:
        reservoir = Reservoir(dataset)

    is_meth = meth_id is not None
    for _, row in df.iterrows():
        read_id = row['read_id']

        filename = "{}/{}/{}/{}.fast5".format(conf['base_dir'], row['library'],
                                              row['meth_group'], read_id)
        try:
            raw_signal, seg_starts, bases = get_base_seg(filename, row)
        except Exception as e:
            logging.error("Failed to parse: %s; Exception: %s", filename, e)
            continue

        # Split dataset only into windows.
        if args.read_split:
            reservoir.add(raw_signal, seg_starts, bases, row)
            continue

        # Split read into windows.
        window_generator = partial(gen_control_window, read_id=read_id,
                                   conf=conf)
        if is_meth:
            window_generator = partial(
                gen_meth_window, read_id=read_id,
                meth_pattern=METH_PATTERNS[meth_id], conf=conf)

        for win_signal, csv_line, seg_starts in window_generator(
                raw_signal, seg_starts, bases):
            csv_line.append(row["strand"])
            reservoir.add(win_signal, csv_line, seg_starts)

    if args.read_split:
        return reservoir.get_stats_line()
    else:
        return reservoir.save(dataset_desc)


random.seed(conf["random_seed"])

df = pd.read_csv(conf['tombo_stats_csv'])

# Split the reads into train/dev/test based on reference position.
df = df[df['meth_group'].isin(conf['groups'])]
df['dataset'] = df.apply(get_dataset_for_read, axis=1)
df['is_meth'] = df.apply(lambda row: row['meth_group'].startswith('meth'),
                         axis=1)

across_boundaries = df['dataset'].isnull()
logging.info("Number of reads that mapped across boundaries: %d",
             across_boundaries.sum())
df = df[~across_boundaries]

is_short = df['total_seg_len_sum'] <= conf['window_size']
logging.info("Number of short reads: %d", is_short.sum())
df = df[~is_short]

# Sort data frame by read id to preserve the order of reads.
df = df.sort_values(by='read_id')

os.makedirs(conf['output_dir'], exist_ok=True)

grouped = df.groupby(['meth_group', 'dataset'])
groups = []
for (meth_group, dataset), df in grouped:
    # Skip datasets for which we want zero samples.
    if conf[dataset + "_samples"] == 0:
        continue

    if meth_group.startswith("meth"):
        groups.append((meth_group, dataset, df, meth_group))
        continue

    # Append control group without centering window on any methylation pattern.
    groups.append((meth_group, dataset, df, None))

    # Cetering of windows does not make sense for full reads.
    if not args.read_split:
        # Append control group samples with all the meth. modifications.
        # We want to create also windows from control samples centered on the
        # individual methylation pattern so we can use them in cross-validation.
        for group in conf['groups']:
            if group.startswith("meth"):
                groups.append((meth_group, dataset, df, group))

pool = Pool()
csv_lines = [summary for summary in tqdm.tqdm(
    pool.imap_unordered(split_dataset, groups),
    total=len(groups), file=sys.stdout)]

header = "meth_group,dataset,total_samples,total_bases,meth_id"
summary_file = "{}/{}.csv".format(conf['output_dir'], "summary")
with open(summary_file, 'w+') as f:
    f.write("\n".join([header] + csv_lines))
    f.write("\n")

copyfile(args.yaml_config, conf["output_dir"] + "/split_conf.yaml")
