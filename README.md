Dependencies
============

* [bwa](https://github.com/lh3/bwa)
* [graphmap](https://github.com/isovic/graphmap)
* [tombo](https://github.com/nanoporetech/tombo)
* [minimap2](https://github.com/lh3/minimap2)
* [poretools](https://github.com/arq5x/poretools)
* [scrappie](https://github.com/nanoporetech/scrappie)
* [samtools](https://github.com/samtools/samtools.git)
* GNU parallel

Python dependecies are in `requirements.txt`. Install them using:

```
pip3 install -r requirements.txt
```
