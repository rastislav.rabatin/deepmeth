import os
import re
import yaml
import time
import logging

import numpy as np
import pandas as pd

from collections import namedtuple

# Forward strand methylation patterns.
# For all of the pattern this holds true:
# base[i] == complement(base[-i])
# Legend:
# Y = C or T (pyrimidine)
# R = A or G (purine)
# W = weak A or T
METH_PATTERNS = {
    "meth01": "TCGA",
    "meth02": "GGATCC",
    "meth03": "GGCGCC",
    "meth04": "GAATTC",
    "meth05": "TCTAGA",
    "meth06": "WCCGGW",
    "meth07": "GTYRAC",
    "meth08": "GCGC",
    "meth09": "CG",
    "meth10": "CG",
    "meth11": "GATC",
    "meth12": "GRCGYC",
}

METHYLATED_POSITIONS = {
    "meth01": 3,
    "meth02": 4,
    "meth04": 2,
    "meth08": 1,
    "meth09": 0,
    "meth10": 0,
    "meth11": 1,
}

# Dataset descriptor:
# name: control[0-9]|meth[0-9][0-9]|train|dev|test -- is it methylated or PCRed?
# type: train|dev|test -- 1st, 2nd or 3rd part of the genome.
# meth_id: meth[0-9][0-9]|None -- pattern that it is centered on.
DatasetDesc = namedtuple('DatasetDesc', ['name', 'type', 'meth_id'])

# The number of bases we want to have around the base in the middle of the
# window in the testing phase.
TESTING_BASES_AROUND = 5
# If more than 90 first observations in the window correspond to a base that
# starts outside of the window then discard this window.
TESTING_OBS_BEGIN = 90


def dataset_desc_to_str(dataset_desc):
    return "{}_{}_{}".format(dataset_desc.name, dataset_desc.type,
                             dataset_desc.meth_id)


def epoch_num_to_str(epoch):
    return "epoch_{0:05d}".format(epoch)


def loading_timer(load_fn):
    def wrapper(path):
        logger = logging.getLogger(__name__)
        logger.info("Loading %s ...", path)
        start_time = time.time()
        res = load_fn(path)
        logger.info("Loading of %s took %f sec.", path,
                    time.time() - start_time)

        return res

    return wrapper


@loading_timer
def load_np_array(path):
    with np.load(path) as f:
        res = f['arr_0']
    logging.getLogger(__name__).info("Shape of array in %s is %s", path,
                                     res.shape)

    return res


def get_all_dataset_names(split_conf):
    datasets = []
    methylases = [g for g in split_conf["groups"] if g.startswith("meth")]
    for dataset_name in split_conf["groups"]:
        for dataset_type in ["train", "dev", "test"]:
            if split_conf[dataset_type + "_samples"] == 0:
                continue

            for meth in methylases:
                datasets.append((dataset_name, dataset_type, meth))

                # Samples which are not centered on any methylation pattern.
                if dataset_name.startswith("control"):
                    datasets.append((dataset_name, dataset_type, None))

    return datasets


def get_dataset_path_suffix(dataset_desc):
    name = dataset_desc.name
    type = dataset_desc.type
    meth_id = dataset_desc.meth_id
    path = "/{}/{}".format(name, type)

    if name.startswith("meth"):
        return path

    if meth_id is None:
        path += "/not_centered"
    else:
        # PCR sample centered on particular meth pattern.
        path += "/{}".format(meth_id)

    return path


def concatenate_datasets(dataset_list):
    logging.getLogger(__name__).info("Concatenating datasets...")
    X = np.concatenate(tuple(x for x, y in dataset_list), axis=0)
    y = np.concatenate(tuple(y for x, y in dataset_list), axis=0)

    return X, y


def get_all_control(get_one_control_fn, dataset_type,
                    meth_id):
    return [
        get_one_control_fn(DatasetDesc(g, dataset_type, meth_id))
        for g in ["control1", "control2"]
    ]


def load_bases_dict(path):
    res = {}
    for line in open(path):
        key, value = line.split(",")
        res[key] = value

    return res


# Loads windows from reads.
class DataLoader:
    def __init__(self, split_conf, crop_obs, reshape_for_keras=True):
        self.logger = logging.getLogger(__name__)

        self.crop_obs = crop_obs
        self.split_conf = split_conf
        # Keras needs input shape (samples, time steps, features). We have only
        # one feature so the third dimension is 1. If this flag is false return
        # array of shape (samples, time steps).
        self.reshape_for_keras = reshape_for_keras

    def _get_dataset_path(self, dataset_desc):
        return self.split_conf["output_dir"] + \
               get_dataset_path_suffix(dataset_desc)

    def _get_signal_dataset(self, dataset_desc):
        file_path = self._get_dataset_path(dataset_desc)
        X = load_np_array(file_path + "/signal.npz")
        y = X[:, self.crop_obs:-self.crop_obs]

        if self.reshape_for_keras:
            X = np.reshape(X, (X.shape[0], X.shape[1], 1))
            y = np.reshape(y, (y.shape[0], y.shape[1], 1))

        return X, y

    def _get_seg_starts_dataset(self, dataset_desc):
        path = self._get_dataset_path(dataset_desc) + "/seg_starts.npz"
        return load_np_array(path)

    def _get_bases_df(self, dataset_desc):
        path = self._get_dataset_path(dataset_desc) + "/bases.csv"
        return loading_timer(pd.read_csv)(path)

    def get_meth_data(self, load_fn, dataset_type):
        return {
            group: load_fn(DatasetDesc(group, dataset_type, group))
            for group in self.split_conf["groups"] if group.startswith("meth")
        }

    def get_control_signal_data(self, dataset_type, meth_id=None):
        return concatenate_datasets(
            get_all_control(self._get_signal_dataset, dataset_type,
                            meth_id))

    def get_control_seg_starts(self, dataset_type, meth_id=None):
        data = get_all_control(self._get_seg_starts_dataset,
                               dataset_type, meth_id)
        return np.concatenate(tuple(data), axis=0)

    def get_control_bases(self, dataset_type, meth_id=None):
        data = get_all_control(self._get_bases_df, dataset_type,
                               meth_id)
        return pd.concat(data, axis=0).reset_index(drop=True)

    def get_all_signal_data(self):
        datasets = self.get_meth_data(self._get_signal_dataset, "dev")
        datasets["train"] = self.get_control_signal_data("train")
        datasets["dev"] = self.get_control_signal_data("dev")

        return datasets

    def get_all_seg_starts_data(self):
        datasets = self.get_meth_data(self._get_seg_starts_dataset, "dev")
        datasets["train"] = self.get_control_seg_starts("train")
        datasets["dev"] = self.get_control_seg_starts("dev")

        return datasets

    def get_all_bases_data(self):
        datasets = self.get_meth_data(self._get_bases_df, "dev")
        datasets["train"] = self.get_control_bases("train")
        datasets["dev"] = self.get_control_bases("dev")

        return datasets

    def get_meth_pattern_control_samples(self, dataset_type):
        """Return control samples centered on methylation pattern.
        Returns dict that maps meth pattern to (X, y).
        """
        return {
            (dataset_type, group):
                self.get_control_signal_data(dataset_type, group)
            for group in self.split_conf["groups"] if group.startswith("meth")
        }

    def get_signal_from(self, dataset_desc):
        if dataset_desc.name in ["train", "dev", "test"]:
            return self.get_control_signal_data(dataset_desc.name,
                                                dataset_desc.meth_id)

        return self._get_signal_dataset(dataset_desc)

    def get_bases_from(self, dataset_desc):
        if dataset_desc.name in ["train", "dev", "test"]:
            return self.get_control_bases(dataset_desc.name,
                                          dataset_desc.meth_id)

        return self._get_bases_df(dataset_desc)

    def get_seg_starts_from(self, dataset_desc):
        if dataset_desc.name in ["train", "dev", "test"]:
            return self.get_control_seg_starts(dataset_desc.name,
                                               dataset_desc.meth_id)

        return self._get_seg_starts_dataset(dataset_desc)


# Reads full reads.
class ReadLoader:
    def __init__(self, split_conf):
        self.logger = logging.getLogger(__name__)
        self.split_conf = yaml.load(open(split_conf))

    def _get_dataset_files(self, dataset_desc):
        if dataset_desc.name in ["train", "dev", "test"]:
            control1 = DatasetDesc("control1", dataset_desc.type,
                                   dataset_desc.meth_id)
            control2 = DatasetDesc("control2", dataset_desc.type,
                                   dataset_desc.meth_id)

            return self._get_dataset_files(control1) + \
                   self._get_dataset_files(control2)

        path = self.split_conf['output_dir'] + \
               get_dataset_path_suffix(dataset_desc)

        return ["{}/{}".format(path, filename) for filename in os.listdir(path)]

    def _load_np_arrays(self, dataset_desc, path_suffix):
        return {
            path.split("/")[-1].replace(path_suffix, ""): load_np_array(path)
            for path in self._get_dataset_files(dataset_desc)
            if path.endswith(path_suffix)
        }

    def get_all_signal(self, dataset_desc):
        return self._load_np_arrays(dataset_desc, "_signal.npz")

    def get_all_seg_starts(self, dataset_desc):
        return self._load_np_arrays(dataset_desc, "_seg_starts.npz")

    def get_all_bases(self, dataset_desc):
        return {
            path.split("/")[-1].replace("_bases.csv", ""): load_bases_dict(path)
            for path in self._get_dataset_files(dataset_desc)
            if path.endswith("_bases.csv")
        }

    def num_reads(self, dataset_desc):
        paths = self._get_dataset_files(dataset_desc)
        assert len(paths) % 3 == 0

        return len(paths) // 3

    def iterate_reads(self, dataset_desc):
        paths = sorted(self._get_dataset_files(dataset_desc))
        assert len(paths) % 3 == 0

        for pos in range(0, len(paths), 3):
            bases = load_bases_dict(paths[pos])
            seg_starts = load_np_array(paths[pos + 1])
            signal = load_np_array(paths[pos + 2])

            yield bases, seg_starts, signal


def load_output_data(dataset_desc, model_dir, model_name, epoch, output_dir):
    def load_dataset(dataset_descriptor):
        epoch_str = epoch_num_to_str(epoch)
        data_dir = "{}/{}/{}/{}".format(model_dir, output_dir,
                                        model_name, epoch_str)
        path_suffix = get_dataset_path_suffix(dataset_descriptor)
        return load_np_array(data_dir + path_suffix + ".npz")

    if dataset_desc.name in ["train", "dev", "test"]:
        return np.concatenate(tuple(
            load_dataset(DatasetDesc(name, dataset_desc.type,
                                     dataset_desc.meth_id))
            for name in ["control1", "control2"]
        ), axis=0)

    return load_dataset(dataset_desc)


def get_predictions_for(dataset_desc, output_dir, model_name, epoch):
    return load_output_data(dataset_desc, output_dir, model_name, epoch,
                            "predictions")


def get_bottleneck_for(dataset_desc, output_dir, model_name, epoch):
    return load_output_data(dataset_desc, output_dir, model_name, epoch,
                            "bottleneck")


def filter_windows(seg_starts, win_size):
    """Remove windows which contain less than n_bases_around the center base.
    Returns binary vector indicating whether to remove the sample or not.
    """
    middle = win_size // 2
    skip_sample = [False for _ in range(seg_starts.shape[0])]
    for pos, starts in enumerate(seg_starts):
        if starts[0] > TESTING_OBS_BEGIN:
            skip_sample[pos] = True
            continue

        mid_base = np.searchsorted(starts, middle)

        # This should not happen but skip these samples anyway.
        if mid_base >= len(starts):
            skip_sample[pos] = True
            continue

        # Binary search found the next base.
        # The middle signal is in between two starts.
        if starts[mid_base] != middle:
            mid_base -= 1

        if mid_base + TESTING_BASES_AROUND >= len(starts) or \
                mid_base - TESTING_BASES_AROUND < 0:
            skip_sample[pos] = True
            continue

    return np.array(skip_sample)


def get_errors_for(data_loader, dataset_desc, output_dir, model_name, epoch):
    logger = logging.getLogger(__name__)

    y_pred = get_predictions_for(dataset_desc, output_dir, model_name, epoch)
    X, y_true = data_loader.get_signal_from(dataset_desc)
    bases_df = data_loader.get_bases_from(dataset_desc)
    seg_starts = data_loader.get_seg_starts_from(dataset_desc)

    assert len(seg_starts) == len(y_true)
    assert bases_df.shape[0] == len(y_true)

    skip_sample = filter_windows(seg_starts, X.shape[1])
    n_samples = len(skip_sample)
    n_remove = np.sum(skip_sample)
    logger.warning("Skipping %d (%f %% -- %d) samples for %s", n_remove,
                   (n_remove / n_samples) * 100, n_samples, dataset_desc)

    assert len(y_true) == len(y_pred) or len(y_pred) == np.sum(~skip_sample)

    if len(y_pred) != np.sum(~skip_sample):
        y_pred = y_pred[~skip_sample]
    y_true = y_true[~skip_sample]
    bases_df = bases_df.iloc[~skip_sample, :]

    assert len(y_true) == len(y_pred)
    assert bases_df.shape[0] == len(y_true)

    errs = (y_pred - y_true) ** 2
    return errs.astype(np.float64), bases_df


def get_configs(split_conf_path, train_conf_path):
    return yaml.load(open(split_conf_path)), yaml.load(open(train_conf_path))


def get_configs_from_model_dir(args):
    split_conf_path = args.model_dir + "/split_conf.yaml"
    train_conf_path = args.model_dir + "/train_conf.yaml"
    if args.split_conf is not None:
        split_conf_path = args.split_conf
    if args.train_conf is not None:
        train_conf_path = args.train_conf

    return get_configs(split_conf_path, train_conf_path)


def concatenate_dfs(df_list):
    return pd.concat(df_list, axis=0).reset_index(drop=True)


def get_epoch_num_from(filename):
    res = re.findall(r"epoch_([0-9]+)", filename)
    assert len(res) <= 1

    if len(res) == 0:
        return None

    return int(res[0])


def get_val_loss_from(checkpoint_name):
    res = re.findall(r"val_loss_(\d+\.\d+)", checkpoint_name)
    assert len(res) <= 1

    if len(res) == 0:
        return None

    return float(res[0])


def get_last_epoch(directory):
    return max(get_epoch_num_from(f)
               for f in os.listdir(directory)
               if get_epoch_num_from(f) is not None)


def get_epoch_with_min_val_loss(model_dir):
    df = pd.read_csv(model_dir + "/epochs.csv")
    idx = df["val_loss"].idxmin()
    min_val_loss_row = df.loc[idx, :]

    return min_val_loss_row["val_loss"], int(min_val_loss_row["epoch"]) + 1


def get_filename_with(epoch, directory):
    filename = [f for f in os.listdir(directory)
                if get_epoch_num_from(f) == epoch]
    assert len(filename) == 1

    return filename[0]


def epoch_exists_in(directory, epoch):
    filename = [f for f in os.listdir(directory)
                if get_epoch_num_from(f) == epoch]
    assert len(filename) <= 1

    return len(filename) == 1


def unpack_bases(base_seq, seg_starts, win_size):
    # The first couple observations might not correspond to any base that is
    # fully inside the window.
    res = ["-" for _ in range(win_size)]

    base_pos = 0
    for sig_pos in range(seg_starts[0], win_size):
        if base_pos + 1 < len(seg_starts) and \
                seg_starts[base_pos + 1] == sig_pos:
            base_pos += 1

        res[sig_pos] = base_seq[base_pos]

    return res


def get_skipped(dataset_desc, model_dir):
    """Return indices of the samples that were skipped during dataset
    preparation.
    """
    logger = logging.getLogger(__name__)

    if dataset_desc.name in ["train", "test", "dev"]:
        control1 = get_skipped(DatasetDesc("control1", *dataset_desc[1:]),
                               model_dir)
        control2 = get_skipped(DatasetDesc("control2", *dataset_desc[1:]),
                               model_dir)
        return np.concatenate((control1, control2 + len(control1)))

    # Load skipped samples.
    path = "{}/skipped_samples_{}.txt".format(model_dir,
                                              dataset_desc_to_str(dataset_desc))
    if not os.path.isfile(path):
        logger.warning("Did not find any skipped samples for %s", dataset_desc)
        return None

    return np.array([int(line.strip()) for line in open(path) if line != "\n"],
                    dtype=np.int32)
