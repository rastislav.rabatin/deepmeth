from data_loader import *
from plot_utils import *

from scipy.stats import norm
from functools import partial
from collections import defaultdict
from scipy.ndimage import median_filter
from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.metrics import accuracy_score

from matplotlib.backends.backend_pdf import PdfPages

MAX_COVERAGE = 15


def get_log_odds(errs, meth_distrib_params, pcr_distrib_params,
                 pdf_fn=norm.logpdf):
    """ Return likelihood that the sample is PCRed and that it is methylated
    using one distribution per position and assuming that the positions are
    independent.
    """
    meth_odds = np.zeros(errs.shape, dtype=np.float64)
    pcr_odds = np.zeros(errs.shape, dtype=np.float64)
    for pos in range(errs.shape[1]):
        loc_meth, scale_meth = meth_distrib_params[:, pos]
        loc_pcr, scale_pcr = pcr_distrib_params[:, pos]

        meth_odds[:, pos] = pdf_fn(errs[:, pos], loc=loc_meth, scale=scale_meth)
        pcr_odds[:, pos] = pdf_fn(errs[:, pos], loc=loc_pcr, scale=scale_pcr)

    return pcr_odds.sum(axis=1), meth_odds.sum(axis=1)


def get_scores_df(pcr_errs, meth_errs, pcr_bases_df,
                  meth_bases_df, dataset_label, predict_fn):
    pcr_odds, meth_odds = predict_fn(np.concatenate((pcr_errs, meth_errs)))
    ref_pos = np.concatenate((pcr_bases_df.pattern_ref_pos.values,
                              meth_bases_df.pattern_ref_pos.values))

    sizes = [len(pcr_errs), len(meth_errs)]
    return pd.DataFrame.from_dict({
        'score': meth_odds - pcr_odds,
        'pcr_odds': pcr_odds,
        'meth_odds': meth_odds,
        'pattern_ref_pos': ref_pos,
        'dataset_name': np.repeat([dataset_label + "_pcr", dataset_label],
                                  sizes),
        'is_methylated': np.repeat([False, True], sizes)
    })


def split_by_center_kmer(base_seqs, dataset_desc, kmer_length):
    meth_pattern = METH_PATTERNS[dataset_desc.meth_id]
    pattern_len = len(meth_pattern)
    add_bases = max(0, (kmer_length - pattern_len) // 2)

    kmer_to_indices = defaultdict(list)
    for idx, base_seq in enumerate(base_seqs):
        pos = base_seq.find(meth_pattern)
        start = pos - add_bases
        end = pos + pattern_len + add_bases
        center_kmer = base_seq[start:end]

        if len(center_kmer) != kmer_length:
            logging.error("Skipping window %d in %s. "
                          "Not enough bases to include into pattern.",
                          idx, dataset_desc)
            continue

        kmer_to_indices[center_kmer].append(idx)

    return kmer_to_indices


def add_pattern_ref_pos(bases_df, meth_id, tombo_stats_csv_path):
    """ Add reference positions to bases_df.
    """
    pattern = METH_PATTERNS[meth_id]
    tb_df = pd.read_csv(tombo_stats_csv_path)

    df = bases_df.merge(tb_df[["read_id", "ref_start"]], on='read_id')
    df["pattern_ref_pos"] = \
        df["ref_start"] + df["bases_start"] + \
        df["base_seq"].apply(lambda seq: seq.find(pattern))

    return df


def get_pcr_meth_errs_for(dataset_type, meth_id, split_conf, data_loader,
                          model_name, epoch, med_filt_size, log_trans=False):
    output_dir = split_conf["output_dir"]
    pcr_errs, pcr_bases_df = \
        get_errors_for(data_loader,
                       DatasetDesc(dataset_type, dataset_type, meth_id),
                       output_dir, model_name, epoch)
    meth_errs, meth_bases_df = \
        get_errors_for(data_loader,
                       DatasetDesc(meth_id, dataset_type, meth_id),
                       output_dir, model_name, epoch)

    pcr_errs = median_filter(pcr_errs, size=med_filt_size, mode='nearest')
    meth_errs = median_filter(meth_errs, size=med_filt_size,
                              mode='nearest')
    if log_trans:
        pcr_errs = np.log(pcr_errs)
        meth_errs = np.log(meth_errs)

    tb_stats_csv_path = split_conf["tombo_stats_csv"]
    pcr_bases_df = add_pattern_ref_pos(pcr_bases_df, meth_id, tb_stats_csv_path)
    meth_bases_df = add_pattern_ref_pos(meth_bases_df, meth_id,
                                        tb_stats_csv_path)

    return pcr_errs, meth_errs, pcr_bases_df, meth_bases_df


def fit_model_for(pcr_errs, meth_errs, fit_fn_per_pos=norm.fit):
    pcr_params_per_pos = np.apply_along_axis(fit_fn_per_pos, axis=0,
                                             arr=pcr_errs)
    meth_params_per_pos = np.apply_along_axis(fit_fn_per_pos, axis=0,
                                              arr=meth_errs)

    return partial(get_log_odds, pcr_distrib_params=pcr_params_per_pos,
                   meth_distrib_params=meth_params_per_pos)


def get_kmer_models(kmer_to_pcr_indices, kmer_to_meth_indices, pcr_errs,
                    meth_errs):
    res = {}
    for kmer, pcr_indices in kmer_to_pcr_indices.items():
        meth_indices = kmer_to_meth_indices[kmer]

        if len(pcr_indices) == 0 or len(meth_indices) == 0:
            logging.warning("Skipping kmer %s. Has only samples from one group",
                            kmer)
            continue

        res[kmer] = fit_model_for(pcr_errs[pcr_indices],
                                  meth_errs[meth_indices])

    return res


def get_sample_indices_in_train(train_kmers, kmer_to_indices):
    res = []
    for kmer, indices in kmer_to_indices.items():
        if kmer in train_kmers:
            res += indices

    return res


def get_kmer_scores(kmer_to_pcr_indices, kmer_to_meth_indices, pcr_errs,
                    meth_errs, pcr_bases_df, meth_bases_df,
                    kmer_to_predict_fn, meth_id):
    res = {}
    for kmer, pcr_indices in kmer_to_pcr_indices.items():
        meth_indices = kmer_to_meth_indices[kmer]

        if len(pcr_indices) == 0 or len(meth_indices) == 0:
            logging.warning("Skipping kmer %s. Has only samples from one group",
                            kmer)
            continue

        if kmer not in kmer_to_predict_fn.keys():
            logging.warning("Kmer %s was not in training data. Skipping...",
                            kmer)
            continue

        df = get_scores_df(
            pcr_errs[pcr_indices], meth_errs[meth_indices],
            pcr_bases_df.loc[pcr_indices], meth_bases_df.loc[meth_indices],
            kmer, kmer_to_predict_fn[kmer])
        df["meth_id"] = meth_id
        df["kmer"] = kmer

        res[kmer] = df

    return res


def select_data_for(strand, data):
    pcr_errs, meth_errs, pcr_bases_df, meth_bases_df = data

    pcr_strand = pcr_bases_df.strand == strand
    meth_strand = meth_bases_df.strand == strand

    return (pcr_errs[pcr_strand], meth_errs[meth_strand],
            pcr_bases_df[pcr_strand].reset_index(drop=True),
            meth_bases_df[meth_strand].reset_index(drop=True))


def filter_out_nans(df):
    is_nan_score = df.score.isnull()
    logging.warning("NaNs: %d; Skipping NaN scores...", is_nan_score.sum())
    return df[~is_nan_score]


def compute_auc(df):
    kmer = df.kmer.values[0]

    df = filter_out_nans(df)
    scores = df.score

    # Positive = methylated.
    y_true = df.is_methylated

    if np.all(y_true) or np.all(~y_true):
        logging.warning("Skipping kmer %s. Has only samples from one group",
                        kmer)
        return None

    y_pred = scores > 0
    auc = roc_auc_score(y_true, scores)

    n_positive = np.sum(y_true)
    columns = ["kmer", "meth_id", "auc", "f1", "precision", "recall",
               "accuracy", "pos_samples", "neg_samples"]
    row = [kmer, df.meth_id.values[0], auc, f1_score(y_true, y_pred),
           precision_score(y_true, y_pred), recall_score(y_true, y_pred),
           accuracy_score(y_true, y_pred), n_positive, len(y_true) - n_positive]

    return pd.Series(row, index=columns)


def aggregate_samples(grouped_df, coverage=None):
    COLS = ["site", "meth_id", "is_methylated", "score", "score_mean",
            "score_std", "coverage"]

    aggregated_rows = []
    for (pattern_ref_pos, meth_id, is_methylated), group in grouped_df:
        subsample = group
        if coverage is not None:
            if group.shape[0] < coverage:
                continue

            subsample = group.sample(n=coverage, replace=False)

        score = subsample.score
        row = (pattern_ref_pos, meth_id, is_methylated, score.sum(),
               score.mean(), score.std(), subsample.shape[0])

        assert len(row) == len(COLS)
        aggregated_rows.append(row)

    return pd.DataFrame.from_records(aggregated_rows, columns=COLS)


def plot_curves_for_coverages(filename, score_dfs, plot_curve_fn):
    auc_dfs = []
    with PdfPages(filename) as pdf:
        for coverage, scores_df in score_dfs:
            auc_df = plot_curve_fn(scores_df, show_num_samples=True)
            plt.title("Coverage = {}".format(coverage))
            pdf.savefig()
            plt.close()

            scores_df["coverage"] = coverage
            auc_df["coverage"] = coverage
            auc_dfs.append(auc_df)

    return concatenate_dfs(auc_dfs)


def save_auc_score(site_roc_auc_df, site_pr_auc_df, plots_dir, filename_suffix):
    # Reorder columns.
    COLS = ["meth_id", "pattern", "coverage", "roc_auc", "pr_auc",
            "pos_samples", "neg_samples", "n_samples"]
    order_by = ["meth_id", "coverage"]
    auc_df = site_roc_auc_df.sort_values(order_by)
    auc_df["pr_auc"] = site_pr_auc_df.sort_values(order_by).pr_auc
    auc_df = auc_df[COLS]
    auc_df.to_csv("{}/auc{}.csv".format(plots_dir, filename_suffix),
                  index=False)


def eval_sites(site_scores_dfs, plots_dir, filename_suffix):
    filename = "{}/ref_pos_roc{}.pdf".format(plots_dir, filename_suffix)
    site_roc_auc_df = plot_curves_for_coverages(filename, site_scores_dfs,
                                                plot_roc)

    filename = "{}/ref_pos_pr{}.pdf".format(plots_dir, filename_suffix)
    site_pr_auc_df = plot_curves_for_coverages(filename, site_scores_dfs,
                                               plot_precision_recall)

    plot_score_vs_coverage(site_roc_auc_df, score_col_name="roc_auc")
    plt.ylabel("ROC AUC")
    plt.savefig("{}/roc_vs_coverage{}.png".format(plots_dir, filename_suffix))
    plt.close()

    plot_score_vs_coverage(site_pr_auc_df, score_col_name="pr_auc")
    plt.ylabel("PR AUC")
    plt.savefig("{}/pr_vs_coverage{}.png".format(plots_dir, filename_suffix))
    plt.close()

    return site_roc_auc_df, site_pr_auc_df


def get_balanced_dataset(df):
    res = []
    for _, group in df.groupby("meth_id"):
        meth = group[group.is_methylated]
        pcr = group[~group.is_methylated]
        n_samples = min(meth.shape[0], pcr.shape[0])

        if n_samples == 0:
            continue

        res.append(meth.sample(n_samples, axis=0))
        res.append(pcr.sample(n_samples, axis=0))

    return concatenate_dfs(res)


def eval_balanced_dataset(site_scores_dfs, plots_dir, filename_suffix):
    # Create balanced dataset where for every coverage  we have the same
    # number of positive and negative sites.
    balanced_sites = [(coverage, get_balanced_dataset(df))
                      for coverage, df in site_scores_dfs]

    filename_suffix += "_balanced"

    site_roc_auc_df, site_pr_auc_df = eval_sites(balanced_sites, plots_dir,
                                                 filename_suffix)
    save_auc_score(site_roc_auc_df, site_pr_auc_df, plots_dir, filename_suffix)


def eval_scores(strand, scores_df, plots_dir, filename_suffix=""):
    if filename_suffix != "":
        filename_suffix = "_" + filename_suffix

    scores_df = filter_out_nans(scores_df)

    filename_suffix = "_{}{}".format(strand, filename_suffix)

    # ROC for single reads.
    roc_auc_df = plot_roc(scores_df)
    plt.savefig("{}/single_read_roc{}.png".format(plots_dir, filename_suffix))
    plt.close()

    pr_auc_df = plot_precision_recall(scores_df)
    plt.savefig("{}/single_read_pr{}.png".format(plots_dir, filename_suffix))
    plt.close()

    auc_df = roc_auc_df
    auc_df["pr_auc"] = pr_auc_df.sort_values("meth_id").pr_auc
    auc_df.to_csv("{}/single_read_auc{}.csv".format(plots_dir, filename_suffix),
                  index=False)

    scores_df.to_csv("{}/single_read{}.csv".format(plots_dir, filename_suffix),
                     index=False)

    # Aggregate all methylation scores for one site with the same methylation
    # status. We are mainly interested in the sum of methylations scores
    # since this is going to be a new score for the site.
    grouped_df = \
        scores_df.groupby(["pattern_ref_pos", "meth_id", "is_methylated"])

    site_scores_dfs = [(coverage, aggregate_samples(grouped_df, coverage))
                       for coverage in range(1, MAX_COVERAGE + 1)]

    # Require that at least one sample is methylated and not all ouf the
    # samples are methylated.
    site_scores_dfs = [(coverage, df)
                       for coverage, df in site_scores_dfs
                       if df.shape[0] > 0 and not df.is_methylated.all() and
                       df.is_methylated.any()]

    # Save scores for all sites and all coverages.
    site_scores_df = concatenate_dfs([df for _, df in site_scores_dfs])
    site_scores_df.to_csv("{}/site_scores{}.csv".format(
        plots_dir, filename_suffix), index=False)

    site_roc_auc_df, site_pr_auc_df = eval_sites(site_scores_dfs, plots_dir,
                                                 filename_suffix)

    save_auc_score(site_roc_auc_df, site_pr_auc_df, plots_dir, filename_suffix)

    eval_balanced_dataset(site_scores_dfs, plots_dir, filename_suffix)
