import logging


def get_occurances_of(pattern, bases):
    pattern_occurences = []
    for pattern_base_start in range(len(bases) - len(pattern) + 1):
        pattern_base_end = pattern_base_start + len(pattern)
        if bases[pattern_base_start:pattern_base_end] != pattern:
            continue
        pattern_occurences.append(pattern_base_start)

    return pattern_occurences


def gen_meth_window(raw_signal, seg_starts, bases, read_id, meth_pattern, conf):
    """ Generate methylated sample window from the raw signal.

    Every window should contain only one occurrence of the pattern. Windows
    with more occurrences are discarded.

    Args:
        raw_signal (np.array): Normalized raw signal.
        seg_starts (np.array): Starts of the segments in the signal. First
            value is always zero.
        bases (str): Base sequence corresponding to the segments.
        read_id (str): Identifier of the read.
        meth_pattern (str): Methylation pattern.
        conf (dict): Dictionary with configs for the window iterator.

    Yields:
        signal (np.array): Raw signal of length conf['window_size']
        csv_line (list): List with base sequence, and 4 ints - coordinates of
            the window in base sequence and signal sequence.
    """
    signal_win_size = conf['window_size']
    pattern_occurrences = get_occurances_of(meth_pattern, bases)

    # Go over all the occurrences of the pattern and pick those ones that are
    # the only ones in window of size signal_win_size centered at the pattern.
    for pos, base_pos in enumerate(pattern_occurrences):
        # Skip patterns that are at the end of the read.
        if base_pos + len(meth_pattern) >= len(bases):
            break

        # Closed-open interval.
        pattern_signal_start = seg_starts[base_pos]
        pattern_signal_end = seg_starts[base_pos + len(meth_pattern)]

        # Distance from the pattern to the left of the current pattern.
        left_dist = pattern_signal_start
        if pos - 1 >= 0:
            left_occurrence = pattern_occurrences[pos - 1]
            left_occ_signal_end = seg_starts[
                left_occurrence + len(meth_pattern)]
            if pattern_signal_start <= left_occ_signal_end:
                continue

            left_dist = pattern_signal_start - left_occ_signal_end

        # Distance from the pattern which is to the right of the current
        # pattern.
        right_dist = pattern_signal_end
        if pos + 1 < len(pattern_occurrences):
            right_occurence = pattern_occurrences[pos + 1]
            if seg_starts[right_occurence] <= pattern_signal_end:
                continue

            right_dist = seg_starts[right_occurence] - pattern_signal_end

        # Check if the distance to the right and left is large enough.
        pattern_signal_len = pattern_signal_end - pattern_signal_start
        half = (signal_win_size - pattern_signal_len) // 2 + 1
        if right_dist <= half or left_dist <= half:
            continue

        if pattern_signal_len >= signal_win_size:
            logging.warning("Length of the pattern (%s) for read %s starting on"
                            " base position %d is %d samples long. Skipping "
                            "the window.", meth_pattern, read_id, base_pos,
                            pattern_signal_len)
            continue

        # Compute the start and end of the signal window.
        sig_left = (signal_win_size - pattern_signal_len) // 2
        sig_right = signal_win_size - pattern_signal_len - sig_left
        sig_win_start = pattern_signal_start - sig_left
        sig_win_end = pattern_signal_end + sig_right

        if sig_win_end >= len(raw_signal):
            break

        # Compute the start and the end of the window in base coordinates.
        bases_win_start = base_pos
        while bases_win_start > 0 and \
                seg_starts[bases_win_start] >= sig_win_start:
            bases_win_start -= 1
        bases_win_start += 1

        # Keep the end of the last base (bases_win_end-1) inside
        # [sig_win_start, sig_win_end).
        bases_win_end = base_pos + len(meth_pattern)
        while bases_win_end < len(bases) and \
                seg_starts[bases_win_end] - 1 < sig_win_end:
            bases_win_end += 1
        bases_win_end -= 1

        # It's the last window and it's going outside of the read or right at
        # the end of the read. Skip it.
        if bases_win_end == len(bases) - 1:
            break

        # The last base inside the window ends at seg_starts[bases_win_end] - 1.
        assert seg_starts[bases_win_start] >= sig_win_start and \
               seg_starts[bases_win_end] - 1 < sig_win_end

        assert len(raw_signal[sig_win_start:sig_win_end]) == signal_win_size

        csv_line = [read_id, bases[bases_win_start:bases_win_end],
                    bases_win_start, bases_win_end, sig_win_start, sig_win_end]

        segments = seg_starts[bases_win_start:bases_win_end] - sig_win_start

        yield raw_signal[sig_win_start:sig_win_end], csv_line, segments


def gen_control_window(raw_signal, seg_starts, bases, read_id, conf):
    """ Generate control sample window from the raw signal.

    Args:
        raw_signal (np.array): Normalized raw signal.
        seg_starts (np.array): Starts of the segments in the signal. First
            value is always zero.
        bases (str): Base sequence corresponding to the segments.
        read_id (str): Identifier of the read.
        conf (dict): Dictionary with configs for the window iterator.

    Yields:
        signal (np.array): Raw signal of length conf['window_size']
        csv_line (list): List with base sequence, and 4 ints - coordinates of
            the window in base sequence and signal sequence.
    """
    logger = logging.getLogger(__name__)
    sig_win_size = conf['window_size']

    bases_win_start = 0
    bases_win_end = 0
    for sig_win_start in range(0, len(raw_signal), conf['stride']):
        # Using closed-open intervals.
        sig_win_end = sig_win_start + sig_win_size

        while bases_win_end < len(bases) and \
                seg_starts[bases_win_end] - 1 < sig_win_end:
            bases_win_end += 1

        # Keep the end of the last base (bases_win_end-1) inside
        # [sig_win_start, sig_win_end).
        bases_win_end -= 1

        # Again closed-open intervals.
        while bases_win_start < bases_win_end and \
                seg_starts[bases_win_start] < sig_win_start:
            bases_win_start += 1

        # It's the last window and it's going outside of the read or
        # it's right at the end of read. Skip it.
        if bases_win_end == len(bases) - 1 or sig_win_end >= len(raw_signal):
            break

        assert len(raw_signal[sig_win_start:sig_win_end]) == sig_win_size

        if bases_win_end - bases_win_start < conf['win_bases_threshold']:
            logger.warning("Window in read %s with signal coordinates "
                           "(%d, %d) and base coordinates (%d, %d) has "
                           "only %d bases and was discarded.", read_id,
                           sig_win_start, sig_win_end, bases_win_start,
                           bases_win_end, bases_win_end-bases_win_start)
            continue

        # The last base inside the window ends at seg_starts[bases_win_end] - 1.
        assert seg_starts[bases_win_start] >= sig_win_start and \
               seg_starts[bases_win_end] - 1 < sig_win_end

        segments = seg_starts[bases_win_start:bases_win_end] - sig_win_start

        csv_line = [read_id, bases[bases_win_start:bases_win_end],
                    bases_win_start, bases_win_end, sig_win_start, sig_win_end]

        yield raw_signal[sig_win_start:sig_win_end], csv_line, segments
