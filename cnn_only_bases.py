from keras.layers import *
from keras.models import Model

from nn_utils import *


def apply_conv_module(x, n_filters):
    x = Conv1D(filters=n_filters, kernel_size=8, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = BatchNormalization()(x)
    return Activation('elu')(x)


def compile_model(train_conf, window_size):
    n_crop_obs = train_conf["crop_obs"]
    kmer_size, _, _ = get_kmer_size_from_config(train_conf)
    output_size = window_size - 2*n_crop_obs

    bases = Input(shape=(kmer_size * ALPHABET_SIZE,))

    second_layer_width = train_conf["second_layer_width"]
    x = Dense(units=second_layer_width * train_conf["filters"], use_bias=True,
              activation='tanh', kernel_initializer='glorot_normal')(bases)
    x = Reshape(target_shape=(second_layer_width, train_conf["filters"]))(x)

    n_modules = int(np.log2(window_size // second_layer_width))
    for _ in range(n_modules):
        x = apply_conv_module(x, train_conf["filters"])
        x = UpSampling1D(size=2)(x)

    # Last convolutional layer with only one filter. The shape of x should be
    # (512, train_conf["filters"]).
    x = Conv1D(filters=1, kernel_size=8, strides=1, padding='same',
               kernel_initializer='glorot_normal', use_bias=False)(x)
    x = Activation('linear', name='linear')(x)

    x = Cropping1D(cropping=n_crop_obs)(x)
    predictions = Reshape((output_size,))(x)

    model = Model(inputs=bases, outputs=predictions)
    model.compile(optimizer=Adam(lr=train_conf["learning_rate"]),
                  loss='mean_squared_error')

    return model


def prepare_data(dataset_desc, data_loader, train_conf, model_dir):
    inputs, outputs = prepare_signal_with_bases(dataset_desc, data_loader,
                                                train_conf, model_dir)

    # Only k-mers are on input.
    return inputs[1], outputs


def train(model, train_conf, data_loader, model_dir, initial_epoch=0):
    return train_without_meth_val(model, train_conf, data_loader, model_dir,
                                  initial_epoch, prepare_data)
