import inspect
import sharedmem

from scores_utils import *
from utils import NoDeamonPool, strand_to_full_str

from scipy.stats import multivariate_normal, norm, invgauss, gamma, skewnorm
from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM


def fit_diag_norm(pcr_errs):
    params_per_pos = np.apply_along_axis(norm.fit, axis=0, arr=pcr_errs)

    mean = params_per_pos[0, :]
    cov = np.diag(params_per_pos[1, :] ** 2)

    return multivariate_normal(mean, cov).logpdf


def predict_log_normal(errs, predict_norm_fn):
    return predict_norm_fn(np.log(errs))


def fit_diag_log_norm(pcr_errs):
    predict_fn = fit_diag_norm(np.log(pcr_errs))

    return partial(predict_log_normal, predict_norm_fn=predict_fn)


def fit_full_norm(pcr_errs):
    mean = np.mean(pcr_errs, axis=0)
    cov = np.cov(pcr_errs.T)

    return multivariate_normal(mean, cov, allow_singular=True).logpdf


def predict(errs, params_per_pos, log_pdf_fn, select_middle):
    positions = range(errs.shape[1])
    if select_middle:
        positions = range(100, 250)

    res = np.zeros(errs.shape)
    for pos in positions:
        res[:, pos] = log_pdf_fn(errs[:, pos], *params_per_pos[:, pos])

    return res.sum(axis=1)


def fit_distrib_per_pos(pcr_errs, distrib, params_file, select_middle=False):
    if os.path.isfile(params_file):
        params_per_pos = load_np_array(params_file)
    else:
        params_per_pos = np.apply_along_axis(distrib.fit, axis=0, arr=pcr_errs)
        np.savez_compressed(params_file, params_per_pos)

    return partial(predict, params_per_pos=params_per_pos,
                   log_pdf_fn=distrib.logpdf, select_middle=select_middle)


def fit_inv_gauss(pcr_errs, params_file):
    return fit_distrib_per_pos(pcr_errs, invgauss, params_file)


def fit_inv_gauss_middle(pcr_errs, params_file):
    return fit_distrib_per_pos(pcr_errs, invgauss, params_file,
                               select_middle=True)


def fit_inv_gauss_robust(pcr_errs, params_file):
    n_samples = pcr_errs.shape[0]
    n_remove = n_samples // 10
    n_remove_left = n_remove // 2
    n_remove_right = n_remove - n_remove_left
    filtered = np.zeros((n_samples - n_remove, pcr_errs.shape[1]))
    for pos in range(pcr_errs.shape[1]):
        sorted_idx = np.argsort(pcr_errs[:, pos])
        keep_idx = sorted_idx[n_remove_left:-n_remove_right]
        filtered[:, pos] = pcr_errs[keep_idx, pos]

    return fit_distrib_per_pos(filtered, invgauss, params_file,
                               select_middle=True)


def fit_gamma(pcr_errs, params_file):
    return fit_distrib_per_pos(pcr_errs, gamma, params_file)


def fit_skew_norm(pcr_errs, params_file):
    return fit_distrib_per_pos(pcr_errs, skewnorm, params_file)


def fit_elip_envelope(pcr_errs):
    return EllipticEnvelope().fit(pcr_errs).decision_function


def fit_forest(pcr_errs):
    n_samples = min(100000, pcr_errs.shape[0])
    subsample = pcr_errs[np.random.choice(pcr_errs.shape[0], size=n_samples,
                                          replace=False)]

    return IsolationForest(verbose=True, n_jobs=3) \
        .fit(subsample).decision_function


def fit_svm(pcr_errs):
    n_samples = min(10000, pcr_errs.shape[0])
    subsample = pcr_errs[np.random.choice(pcr_errs.shape[0], size=n_samples,
                                          replace=False)]

    return OneClassSVM(verbose=True).fit(subsample).decision_function


def mse_mean_middle(pcr_errs, start, end):
    return -pcr_errs[:, start:end].mean(axis=1)


def fit_mse_simple(pcr_errs):
    return partial(mse_mean_middle, start=0, end=pcr_errs.shape[1])


def fit_mse_middle(pcr_errs):
    return partial(mse_mean_middle, start=100, end=250)


# Every fit function returns a prediction function which takes matrix of shape
# (n_samples, n_features) and returns a vector of score of shape (n_samples,).
MODEL_FIT_FNS = [fit_diag_norm, fit_full_norm, fit_svm, fit_forest,
                 fit_inv_gauss, fit_diag_log_norm, fit_gamma, fit_skew_norm,
                 fit_mse_simple, fit_mse_middle, fit_inv_gauss_middle,
                 fit_inv_gauss_robust]
MODEL_NAMES = ["diag_norm", "full_norm", "svm", "forest", "inv_gauss",
               "log_norm", "gamma", "skew_norm", "mse_simple", "mse_middle",
               "inv_gauss_middle", "inv_gauss_robust"]


def get_anomaly_scores_df(model_name, predict_fn, pcr_errs, meth_errs,
                          pcr_bases_df, meth_bases_df, meth_id):
    logging.info("Computing anomaly score from model %s for %s ...",
                 model_name, meth_id)
    start_time = time.time()

    scores = -predict_fn(np.concatenate((pcr_errs, meth_errs)))

    # Scikit-learn anomaly detection methods return column vector instead of
    # row vector.
    if len(scores.shape) == 2:
        scores = scores[:, 0]

    ref_pos = np.concatenate((pcr_bases_df.pattern_ref_pos.values,
                              meth_bases_df.pattern_ref_pos.values))

    sizes = [len(pcr_errs), len(meth_errs)]
    df = pd.DataFrame.from_dict({
        'score': scores,
        'pattern_ref_pos': ref_pos,
        'dataset_name': np.repeat([meth_id + "_pcr", meth_id], sizes),
        'is_methylated': np.repeat([False, True], sizes)
    })

    df["meth_id"] = meth_id

    logging.info("Finished computing scores from model %s for %s. "
                 "Elapsed %s sec.", model_name, meth_id,
                 time.time() - start_time)

    return model_name, df


def scores_for_strand(meth_id, train_data, dev_data, strand, plots_dir, models):
    """Return list of score dfs for every model.
    """
    pcr_errs, pcr_bases_df = train_data

    with sharedmem.MapReduce() as pool:
        def fit_model(model):
            model_name, fit_fn = model

            # Remove the '_middle' suffix in the parameters filename
            params_dir = "{}/{}/params".format(
                plots_dir, model_name.replace("_middle", ""))
            os.makedirs(params_dir, exist_ok=True)
            params_file = "{}/{}_{}.npz".format(params_dir, meth_id,
                                                strand_to_full_str(strand))

            # Prepare params for the fit function.
            kwargs = {}
            if "params_file" in inspect.signature(fit_fn).parameters:
                kwargs["params_file"] = params_file

            logging.info("Fitting %s for %s ...", model_name, meth_id)
            start_time = time.time()
            if len(kwargs) > 0:
                predict_fn = fit_fn(pcr_errs, **kwargs)
            else:
                predict_fn = fit_fn(pcr_errs)
            logging.info("Fitted %s for %s. Elapsed %s sec.", model_name,
                         meth_id, time.time() - start_time)

            return model_name, predict_fn

        models = [(model_name, fit_fn)
                  for model_name, fit_fn in zip(MODEL_NAMES, MODEL_FIT_FNS)
                  if model_name in models]
        model_predict_fns = pool.map(fit_model, models)

        pcr_errs, meth_errs, pcr_bases_df, meth_bases_df = dev_data

        def compute_score(model):
            model_name, predict_fn = model
            return get_anomaly_scores_df(model_name, predict_fn, pcr_errs,
                                         meth_errs, pcr_bases_df, meth_bases_df,
                                         meth_id)

        res = pool.map(compute_score, model_predict_fns)

    return dict(res)


