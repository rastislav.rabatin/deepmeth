#!/usr/bin/env python3
import logging
import argparse

from nn_utils import *
from shutil import copyfile


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Train neural network on control samples.
    ''')
    parser.add_argument('--split_conf', type=str, default="split_conf.yaml",
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str, default="train_conf.yaml",
                        help='Yaml config file with training configuration.')
    return parser.parse_args()


def make_model_dir(split_conf, train_conf):
    dir_name = split_conf["output_dir"] + "/models/"
    os.makedirs(dir_name, exist_ok=True)
    if "model_name" in train_conf:
        dir_name += train_conf["model_name"] + "_"
    dir_name += time.strftime("%Y%m%d-%H%M%S", time.localtime())
    os.makedirs(dir_name)

    return dir_name


if __name__ == "__main__":
    args = parse_args()
    split_conf, train_conf = get_configs(args.split_conf, args.train_conf)
    output_dir = make_model_dir(split_conf, train_conf)
    logging.basicConfig(filename=output_dir + '/training.log',
                        level=logging.DEBUG)
    copyfile(args.split_conf, output_dir + "/split_conf.yaml")
    copyfile(args.train_conf, output_dir + "/train_conf.yaml")

    apply_tf_settings(train_conf["intra_op_threads"],
                      train_conf["inter_op_threads"])

    model_module = __import__(train_conf["model_module"])
    compile_model = getattr(model_module, 'compile_model')
    train = getattr(model_module, 'train')
    model = compile_model(train_conf, split_conf["window_size"])

    save_model_summary(model, output_dir)
    save_model_structure(model, output_dir)

    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)
    train(model, train_conf, data_loader, output_dir)
