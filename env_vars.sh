export NANORAW="/extdata/nanoraw-data"
export SCRIPTS="$HOME/deepmeth/deepmeth_analysis"
export LOGS="$HOME/deepmeth/deepmeth_analysis/logs"
export REF="$NANORAW/ref_seq/ecoli.fasta"
export TOMBO="$HOME/ont-repos/tombo/tombo"
export TESTS="$HOME/test_reads"

export PATH=$SCRIPTS:$PATH

# Matplotlib backend with not need for $DISPLAY.
export MPLBACKEND="agg"

alias cae16='model_dir="autoencoder_bottleneck16_20180301-145018"; epoch=2736'
alias cae32='model_dir="autoencoder_bottleneck32_20180124-004356"; epoch=2079'
alias cae64='model_dir="neck64_no_bases_20180320-202205"; epoch=1809'

alias dae16='model_dir="mlp16_20180331-165604"; epoch=1989'
alias dae32='model_dir="mlp_bases_20180319-161642"; epoch=4272'
alias dae64='model_dir="mlp_bases_20180331-165151"; epoch=2686'

alias cae_bases32='model_dir="bases32_20180321-143419"; epoch=2292'
alias cae_enc_bases32='model_dir="bases_neck32_no_neck_bases_20180311-203418"; epoch=2082'
