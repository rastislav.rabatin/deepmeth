#!/usr/bin/env python3
import pysam
import argparse


parser = argparse.ArgumentParser(description='''
Check if sum of lengths of the M/I/S/=/X operations shall equal the length of SEQ.
''')
parser.add_argument('sam_file', type=str, help='SAM file.')
args = parser.parse_args()


sam_file = pysam.AlignmentFile(args.sam_file)

for alignment in sam_file:
    cigar = alignment.cigartuples
    cigar_stats = alignment.get_cigar_stats()[0]
    print(list(cigar_stats))
    print(sum(list(cigar_stats[:-1])) - cigar_stats[2])
    print(len(alignment.query_sequence))
    print(alignment.query_sequence)
