#!/usr/bin/env python3
import sys
import argparse

from plot_utils import *
from data_loader import *

from tqdm import tqdm
from functools import partial
from multiprocessing import Pool
from collections import defaultdict
from scipy.ndimage.filters import median_filter


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Plot summary plots over all samples in the dataset group.
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('epoch', type=int,
                        help='Number of the epoch which we want to visualize.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--med_filt_size', type=int, default=5,
                        help='Size of median filter kernel which is used for '
                             'smoothing the error signal.')
    parser.add_argument('--n_windows', type=int, default=100,
                        help='Number of random windows to plot in one figure.')
    parser.add_argument('--log_trans_err', action="store_true", default=False,
                        help='Apply log-transform to error and plot it.')
    return parser.parse_args()


def plot_stats_per_pos(data, plots_dir, y_lim, strand):
    meth_id, dataset_to_stats = data

    if meth_id is None:
        meth_id = "pcr"

    def plot_comparison_of(meth_id, cmp_groups, filename_sufix=None):
        stats_fn_names = ["mean", "median", "Q1", "Q3"]
        stats_color = ["red", "black", "blue", "green"]

        title = get_pretty_meth_label(meth_id)
        plt.suptitle(title)
        plt.ylim(y_lim)
        for dataset_name, stats_per_pos in dataset_to_stats:
            if dataset_name not in cmp_groups:
                continue

            for stat_name, color, vals in zip(stats_fn_names,
                                              stats_color, stats_per_pos):
                label = "{} {}".format(dataset_name.replace("meth", "m"),
                                       stat_name)
                if label.startswith("dev") or label.startswith("train"):
                    label = "neg-" + label

                # label = get_pretty_meth_label(dataset_name) + " " + stat_name

                line_style = 'dashed' if dataset_name == 'dev' else 'solid'

                plt.plot(vals, label=label, linestyle=line_style, color=color)
                plt.legend(loc='upper right')

        plt.xlabel("Position in the output window")
        plt.ylabel("Reconstruction error")
        filename = "{}/stats_per_pos_{}_{}".format(plots_dir, meth_id, strand)
        if filename_sufix is not None:
            filename += "_" + filename_sufix
        filename += ".png"
        plt.savefig(filename)
        plt.close()

    plot_comparison_of(meth_id, ["train", "dev"], "pcr")
    plot_comparison_of(meth_id, ["dev", meth_id])


def plot_multiple_windows(err, plots_dir, dataset_name, strand):
    total_samples = err.shape[0]
    subsample = np.random.choice(range(total_samples),
                                 min(args.n_windows, total_samples),
                                 replace=False)
    err = err[subsample]

    subtitle = get_title(dataset_name)
    filename = "{}/multiple_windows_{}_{}.png".format(plots_dir, dataset_name,
                                                      strand)
    for e in err:
        plt.plot(e, color='black')

    plt.suptitle("Multiple error windows in one plot")
    plt.title(subtitle + " " + strand)

    plt.savefig(filename)
    plt.close()


def get_stats_for_strand(err, meth_id, dataset_name, strand, plots_dir):
    if err.shape[0] == 0:
        logging.warning("Skipping (%s, %s) for strand %s. No samples.",
                        dataset_name, meth_id, strand)
        return None

    logging.info("Number of samples in (%s, %s) for strand %s: %d",
                 dataset_name, meth_id, strand, err.shape[0])

    stats_fn = [np.mean, np.median, partial(np.percentile, q=25),
                partial(np.percentile, q=75)]

    stats_per_pos = [fn(err, axis=0) for fn in stats_fn]
    plot_multiple_windows(err, plots_dir, dataset_name, strand)

    mse_df = pd.DataFrame.from_dict({'score': np.mean(err, axis=1)})

    mse_df["dataset_name"] = dataset_name
    mse_df["is_methylated"] = dataset_name.startswith("meth")
    mse_df["meth_id"] = meth_id

    np.savez_compressed("{}/{}_{}_{}.npz".format(plots_dir, dataset_name,
                                                 meth_id, strand))

    return mse_df, stats_per_pos


def compute_stats_for(dataset_desc, plots_dir):
    err, bases_df = get_errors_for(data_loader, dataset_desc,
                                   split_conf["output_dir"], model_name,
                                   args.epoch)
    strand = bases_df.strand
    err = median_filter(err, size=args.med_filt_size, mode='nearest')

    if args.log_trans_err:
        err = np.log(err)

    meth_id = dataset_desc.meth_id
    dataset_name = dataset_desc.name
    fwd = get_stats_for_strand(err[strand == "+"], meth_id, dataset_name,
                               "fwd", plots_dir)
    rev = get_stats_for_strand(err[strand == "-"], meth_id, dataset_name,
                               "rev", plots_dir)
    mix = get_stats_for_strand(err, meth_id, dataset_name, "both",
                               plots_dir)

    return fwd, rev, mix


def get_plot_limits(dataset_stats):
    min_y_lim = 1000
    max_y_lim = -1000
    for dataset in dataset_stats:
        for data_for_strand in dataset:
            if data_for_strand is None:
                continue

            stats = data_for_strand[1]
            min_y_lim = min(min_y_lim, min(stat.min() for stat in stats))
            max_y_lim = max(max_y_lim, max(stat.max() for stat in stats))

    return min_y_lim, max_y_lim


def mse_plots(mse_df, strand):
    def rename_datasets(row):
        if row["meth_id"] is None or row["dataset_name"] == row["meth_id"]:
            return row["dataset_name"]
        else:
            return "{}_{}".format(row["meth_id"], row["dataset_name"])

    mse_df["dataset_name"] = mse_df.apply(rename_datasets, axis=1)
    groups_order = sorted(g for g in mse_df.dataset_name.unique().tolist()
                          if g.startswith("meth"))
    groups_order += ["train", "dev"]

    pyplot_configs()

    mse_df.to_csv("{}/mse_df_{}.csv".format(plots_dir, strand),
                  index=False)

    mse_df.hist(by='dataset_name', bins=50)
    plt.tight_layout()
    plt.savefig("{}/mse_hists_{}.png".format(plots_dir, strand))
    plt.close()

    roc_auc_df = plot_roc(mse_df)
    plt.savefig("{}/mse_roc_{}.png".format(plots_dir, strand))
    plt.close()

    pr_auc_df = plot_precision_recall(mse_df)
    plt.savefig("{}/mse_pr_{}.png".format(plots_dir, strand))
    plt.close()
    auc_df = pr_auc_df
    auc_df["roc_auc"] = roc_auc_df["roc_auc"]
    auc_df.to_csv("{}/mse_auc_{}.csv".format(plots_dir, strand), index=False)

    # Seaborn plots -- we do not want to change the style of matplotlib plots.
    import seaborn as sns

    pyplot_configs()

    def get_hue(dataset_name):
        if dataset_name.endswith("dev"):
            return "neg-dev"
        elif dataset_name.endswith("train"):
            return "neg-train"
        else:
            return "meth-dev"

    def get_meth(dataset_name):
        name = dataset_name.split('_')[0]
        if name.startswith("meth"):
            return name.replace("meth", "m")
        else:
            return "none"

    mse_df["Subset for a Motif"] = mse_df["dataset_name"].apply(get_hue)
    mse_df["Motif"] = mse_df["dataset_name"].apply(get_meth)
    sns.boxplot(x='Motif', y='score', notch=True, data=mse_df,
                showfliers=False, hue='Subset',
                order=sorted(mse_df["Motif"].unique()),
                hue_order=["meth", "neg-dev", "neg-train"])
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig("{}/mse_boxplots_{}.png".format(plots_dir, strand))
    plt.close()

    sns.violinplot(x='dataset_name', y='score', notch=True, data=mse_df,
                   order=groups_order)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig("{}/mse_violinplots_{}.png".format(plots_dir, strand))
    plt.close()

    g = sns.FacetGrid(mse_df, col='dataset_name', col_wrap=3)
    g.map(sns.distplot, "score")
    plt.savefig("{}/mse_kdeplots_{}.png".format(plots_dir, strand))
    plt.close()


if __name__ == "__main__":
    args = parse_args()
    pyplot_configs()
    logging.basicConfig(stream=sys.stdout,
                        level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)
    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)

    epoch_str = epoch_num_to_str(args.epoch)
    model_name = args.model_dir.split('/')[-1]
    plots_dir = "{}/summary_plots/{}/{}".format(split_conf["output_dir"],
                                                model_name, epoch_str)
    os.makedirs(plots_dir, exist_ok=True)

    # Compute MSE scores for each sample in every dataset.
    pool = Pool()
    dataset_names = [DatasetDesc(g, "dev", g)
                     for g in split_conf["groups"] if g.startswith("meth")]
    dataset_names += [DatasetDesc("dev", "dev", g)
                      for g in split_conf["groups"] if g.startswith("meth")]
    dataset_names += [DatasetDesc("train", "train", g)
                      for g in split_conf["groups"] if g.startswith("meth")]
    dataset_names += [DatasetDesc("train", "train", None),
                      DatasetDesc("dev", "dev", None)]

    fn = partial(compute_stats_for, plots_dir=plots_dir)
    res = list(tqdm(pool.imap_unordered(fn, dataset_names),
                    total=len(dataset_names)))

    min_y_lim, max_y_lim = get_plot_limits(res)

    async_res = []
    for idx, strand in enumerate(["fwd", "rev", "both"]):
        strand_res = [v[idx] for v in res if v[idx] is not None]

        meth_id_to_stats = defaultdict(list)
        # Iterate through data for individual datasets.
        for mse_df, stats in strand_res:
            meth_id = mse_df.meth_id.values[0]
            dataset_name = mse_df.dataset_name.values[0]
            meth_id_to_stats[meth_id].append((dataset_name, stats))

        fn = partial(plot_stats_per_pos, plots_dir=plots_dir,
                     y_lim=(min_y_lim, max_y_lim), strand=strand)
        async_res.append(pool.map_async(fn, meth_id_to_stats.items()))

        mse_df = concatenate_dfs([mse_df for mse_df, _ in strand_res])
        async_res.append(pool.apply_async(mse_plots, (mse_df, strand)))

    for res in tqdm(async_res, total=len(async_res)):
        res.wait()
