#!/usr/bin/env python3
import sys
import argparse

from scores_utils import *

from tqdm import tqdm
from multiprocessing import Pool


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Classify methylation patterns based on log odds ratio score for each 
        read and genome position. Compare model ensemble when we have one 
        model per k-mer with having only one model per methylation pattern.
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('epoch', type=int,
                        help='Number of the epoch which we want to visualize.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--med_filt_size', type=int, default=5,
                        help='Size of median filter kernel which is used for '
                             'smoothing the error signal.')
    parser.add_argument('--kmer_length', type=int, default=6,
                        help='Length of the k-mers which are used for '
                             'comparison.')
    parser.add_argument('--log_trans_err', action="store_true", default=False,
                        help='Apply log-transform to error.')
    return parser.parse_args()


def scores_for_strand(meth_id, train_data, dev_data, kmer_length):
    pcr_errs, meth_errs, pcr_bases_df, meth_bases_df = train_data
    predict_fn = fit_model_for(pcr_errs, meth_errs)

    pcr_kmer_to_indices = split_by_center_kmer(
        pcr_bases_df.base_seq.values,
        DatasetDesc("train", "train", meth_id), kmer_length)
    meth_kmer_to_indices = split_by_center_kmer(
        meth_bases_df.base_seq.values,
        DatasetDesc(meth_id, "train", meth_id), kmer_length)

    kmer_to_predict_fn = get_kmer_models(
        pcr_kmer_to_indices, meth_kmer_to_indices, pcr_errs, meth_errs)

    pcr_errs, meth_errs, pcr_bases_df, meth_bases_df = dev_data

    pcr_kmer_to_indices = split_by_center_kmer(
        pcr_bases_df.base_seq.values, DatasetDesc("dev", "dev", meth_id),
        kmer_length)
    meth_kmer_to_indices = split_by_center_kmer(
        meth_bases_df.base_seq.values, DatasetDesc(meth_id, "dev", meth_id),
        kmer_length)

    keep_pcr_indices = get_sample_indices_in_train(kmer_to_predict_fn.keys(),
                                                   pcr_kmer_to_indices)
    keep_meth_indices = get_sample_indices_in_train(kmer_to_predict_fn.keys(),
                                                    meth_kmer_to_indices)

    scores_df = get_scores_df(pcr_errs[keep_pcr_indices],
                              meth_errs[keep_meth_indices],
                              pcr_bases_df.loc[keep_pcr_indices, :],
                              meth_bases_df.loc[keep_meth_indices, :],
                              meth_id, predict_fn)
    scores_df["meth_id"] = meth_id

    kmer_scores_dfs = get_kmer_scores(pcr_kmer_to_indices, meth_kmer_to_indices,
                                      pcr_errs, meth_errs, pcr_bases_df,
                                      meth_bases_df, kmer_to_predict_fn,
                                      meth_id)

    return scores_df, kmer_scores_dfs


def compute_scores_for(meth_type):
    train_data = get_pcr_meth_errs_for("train", meth_type, split_conf,
                                       data_loader, model_name, args.epoch,
                                       args.med_filt_size, args.log_trans_err)
    dev_data = get_pcr_meth_errs_for("dev", meth_type, split_conf,
                                     data_loader, model_name, args.epoch,
                                     args.med_filt_size, args.log_trans_err)

    res = []
    for strand in ["+", "-"]:
        strand_train_data = select_data_for(strand, train_data)
        strand_dev_data = select_data_for(strand, dev_data)
        res.append(scores_for_strand(meth_type, strand_train_data,
                                     strand_dev_data, args.kmer_length))
    res.append(scores_for_strand(meth_type, train_data, dev_data,
                                 args.kmer_length))

    return res


def compute_auc_df(strand, all_scores_df):
    # WARNING: Pandas calls compute_auc twice on the first group.
    # Be careful with side effects.
    df = all_scores_df.groupby(["kmer", "meth_id"]) \
        .apply(compute_auc) \
        .dropna(axis=0, how='any')
    df["strand"] = strand

    return df


if __name__ == "__main__":
    args = parse_args()
    pyplot_configs()
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)

    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)

    epoch_str = epoch_num_to_str(args.epoch)
    model_name = args.model_dir.split('/')[-1]
    plots_dir = "{}/odds_ratio_kmer/{}/{}".format(split_conf["output_dir"],
                                                  model_name, epoch_str)
    os.makedirs(plots_dir, exist_ok=True)

    pool = Pool()
    meth_types = [g for g in split_conf["groups"] if g.startswith("meth")]
    res = list(tqdm(pool.imap_unordered(compute_scores_for, meth_types),
                    total=len(meth_types)))

    async_res = []

    # Merge all scores dfs and plot ROC curves.
    for idx, strand in enumerate(["fwd", "rev", "both"]):
        scores_df = concatenate_dfs([v[idx][0] for v in res])
        fn_args = (strand, scores_df, plots_dir, "one_model")
        async_res.append(pool.apply_async(eval_scores, fn_args))

    # Merge all score dfs for different meths and k-mers.
    auc_dfs = []
    for idx, strand in enumerate(["fwd", "rev", "both"]):
        # Merge all scores df into one DataFrame.
        dfs = [df for meth_data in res
               for kmer, df in meth_data[idx][1].items()]

        all_scores_df = concatenate_dfs(dfs)
        fn_args = (strand, all_scores_df, plots_dir, "kmers")
        async_res.append(pool.apply_async(eval_scores, fn_args))

        auc_dfs.append(pool.apply_async(compute_auc_df,
                                        (strand, all_scores_df)))

    df = concatenate_dfs([async_res_df.get() for async_res_df in auc_dfs])
    df.to_csv("{}/{}-mer_auc.csv".format(plots_dir, args.kmer_length),
              index=False)

    for r in async_res:
        r.wait()
