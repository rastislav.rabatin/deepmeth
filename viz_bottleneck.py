#!/usr/bin/env python3
import sys
import argparse

from scores_utils import *
from utils import strand_to_full_str, get_random_subsample, NoDeamonPool

from tqdm import tqdm
from multiprocessing import Pool
from sklearn.decomposition import PCA
from sklearn.manifold import MDS, TSNE


def parse_args():
    parser = argparse.ArgumentParser(description='''
        Visualize bottleneck features.
    ''')
    parser.add_argument('model_dir', type=str,
                        help='Directory with model configs and checkpoints.')
    parser.add_argument('epoch', type=int,
                        help='Number of the epoch which we want to visualize.')
    parser.add_argument('--split_conf', type=str,
                        help='Yaml config file used for splitting data.')
    parser.add_argument('--train_conf', type=str,
                        help='Yaml config file with training configuration.')
    parser.add_argument('--tsne_lr', type=str, default=200,
                        help='Learning rate for t-SNE gradient descent.')
    return parser.parse_args()


def subsample_data(data, is_meth, n_samples):
    meth = get_random_subsample(data[is_meth, :], n_samples)
    pcr = get_random_subsample(data[~is_meth, :], n_samples)

    return meth, pcr


def run_dim_red(method, strand, feats, is_meth, meth_id):
    method_name, method_fn = method
    if method_name == "MDS":
        # MDS computes matrix of quadratic size.
        n_samples = 3000
        subsample = np.concatenate(
            subsample_data(feats, is_meth, n_samples=n_samples))
        projected_data = method_fn.fit_transform(subsample)
        is_meth = np.repeat([True, False],
                            [min(n_samples, sum(is_meth)),
                             min(n_samples, sum(~is_meth))])
    else:
        projected_data = method_fn.fit_transform(feats)

    plt.suptitle(method_name)
    plt.title(get_pretty_meth_label(meth_id) + " " + strand)

    meth, pcr = subsample_data(projected_data, is_meth, n_samples=5000)

    plt.scatter(meth[:, 0], meth[:, 1], label="meth", color="red", s=1)
    plt.scatter(pcr[:, 0], pcr[:, 1], label="pcr", color="black", s=1)
    plt.legend(loc='upper right')

    directory = "{}/{}".format(plots_dir, method_name)
    os.makedirs(directory, exist_ok=True)
    plt.savefig("{}/{}_{}.png".format(directory, meth_id, strand))
    plt.close()


def viz_data_for(strand, neck_feats, is_methylated, meth_id):
    dim_reduction_methods = [PCA(n_components=2),
                             MDS(n_components=2, verbose=1),
                             TSNE(learning_rate=args.tsne_lr, verbose=2)]
    methods_names = ["PCA", "MDS", "tSNE"]

    fn = partial(run_dim_red, strand=strand, feats=neck_feats,
                 is_meth=is_methylated, meth_id=meth_id)
    Pool().map(fn, zip(methods_names, dim_reduction_methods))


def load_dataset(dataset_desc):
    neck_feats = get_bottleneck_for(dataset_desc, split_conf["output_dir"],
                                    model_name, args.epoch)
    strand = data_loader.get_bases_from(dataset_desc).strand.values

    # Skip samples that were skipped during dataset preparation for the model.
    skip_samples = get_skipped(dataset_desc, args.model_dir)
    if skip_samples is not None:
        mask = np.full_like(strand, True, dtype=bool)
        mask[skip_samples] = False
        strand = strand[mask]

    return strand, neck_feats.reshape(neck_feats.shape[:2])


def viz_meth(meth_id):
    # Load methylated data centered on meth_id.
    dataset_desc = DatasetDesc(meth_id, "dev", meth_id)
    meth_strand, meth_neck_feats = load_dataset(dataset_desc)

    # Load PCR data centered on meth_id.
    dataset_desc = DatasetDesc("dev", "dev", meth_id)
    pcr_strand, pcr_neck_feats = load_dataset(dataset_desc)

    strand = np.concatenate((meth_strand, pcr_strand))
    neck_feats = np.concatenate((meth_neck_feats, pcr_neck_feats), axis=0)
    is_methylated = np.repeat([True, False],
                              [len(meth_strand), len(pcr_strand)])

    for cur_strand in ["+", "-"]:
        strand_full_name = strand_to_full_str(cur_strand)
        viz_data_for(strand_full_name, neck_feats[strand == cur_strand, :],
                     is_methylated[strand == cur_strand], meth_id)

    viz_data_for("both", neck_feats, is_methylated, meth_id)


if __name__ == "__main__":
    args = parse_args()
    pyplot_configs()
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    split_conf, train_conf = get_configs_from_model_dir(args)

    data_loader = DataLoader(split_conf, train_conf["crop_obs"],
                             reshape_for_keras=False)

    epoch_str = epoch_num_to_str(args.epoch)
    model_name = args.model_dir.split('/')[-1]
    plots_dir = "{}/viz_neck/{}/{}".format(split_conf["output_dir"],
                                           model_name, epoch_str)
    os.makedirs(plots_dir, exist_ok=True)

    pool = NoDeamonPool()
    meth_types = [g for g in split_conf["groups"] if g.startswith("meth")]
    res = list(tqdm(pool.imap_unordered(viz_meth, meth_types),
                    total=len(meth_types)))
